"""parksam URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from common import views as common_views
from consultant import views as consultant_views
from data import views as data_views
from enroll import views as enroll_views
from help import views as help_views
from main import views as main_views
from member import views as member_views
from mypage import views as mypage_views
from program import views as program_views
from django.contrib.auth import views as auth_views
from django.conf.urls import handler400, handler404, handler500

handler400 = "common.views.bad_request_page"
handler404 = "common.views.page_not_found_page"
handler500 = "common.views.server_error_page"

urlpatterns = [
    path("admin/", admin.site.urls),
    path("summernote/", include("django_summernote.urls")),
    path("common/file_upload/", common_views.file_upload, name="file_upload"),
    path("common/file_download/<int:id>/", common_views.file_download, name="file_download"),
    path("send_mail_test/", common_views.send_mail_test, name="send_mail_test"),
    path("terms", common_views.terms, name="terms"),
    path("privacy", common_views.privacy, name="privacy"),
    path("signup/", member_views.signup, name="signup"),
    path("user_check/", member_views.user_check, name="user_check"),
    path("get_info_for_billing/", member_views.get_info_for_billing, name="get_info_for_billing"),
    path("checkplus_success", common_views.checkplus_success, name="checkplus_success"),
    path("checkplus_fail", common_views.checkplus_fail, name="checkplus_fail"),
    path(
        "login/",
        auth_views.LoginView.as_view(template_name="common/login.html"),
        name="login",
    ),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("find_info/", member_views.find_info, name="find_info"),
    path("find_id/", member_views.find_id, name="find_id"),
    path("find_pw/", member_views.find_pw, name="find_pw"),
    # index
    path("", main_views.index, name="index"),
    path("type1/", main_views.index),
    path("type2/", main_views.index),
    path("type3/", main_views.index),
    # common
    path("common/", common_views.test, name="common_test"),
    path("program/", program_views.index, name="program_index"),
    # 회원 컨설팅
    path(
        "member_counseltant/",
        program_views.member_counseltant,
        name="member_counseltant",
    ),
    path(
        "member_counseltant/lite/",
        program_views.member_counseltant_lite,
        name="member_counseltant_lite",
    ),
    # 1회상담
    path(
        "non_member_counseltant/",
        program_views.non_member_counseltant,
        name="non_member_counseltant",
    ),
    path("regular_payment/", program_views.regular_payment, name="regular_payment"),
    path("lite_payment/", program_views.lite_payment, name="lite_payment"),
    path("onetime_payment/", program_views.onetime_payment, name="onetime_payment"),
    path("test_billings/", program_views.test_billings, name="test_billings"),
    path("billings/", program_views.billings, name="billings"),
    path(
        "save_billing_result/",
        program_views.save_billing_result,
        name="save_billing_result",
    ),
    # news
    path("news_list/", data_views.news_list, name="news_list"),
    path("news_detail/<int:id>/", data_views.news_detail, name="news_detail"),
    path("news_new/", data_views.news_new, name="news_new"),
    path("news_form/<int:id>/", data_views.news_form, name="news_form"),
    # column
    path("column_list/", data_views.column_list, name="column_list"),
    # 상담 후기
    path("counsel_review_list/", data_views.counsel_review_list, name="counsel_review_list"),
    path("counsel_review_new/", data_views.counsel_review_new, name="counsel_review_new"),
    path(
        "counsel_review_form/<int:id>/", data_views.counsel_review_form, name="counsel_review_form"
    ),
    # notice
    path("notice_list/", help_views.notice_list, name="notice_list"),
    path("notice_detail/<int:id>/", help_views.notice_detail, name="notice_detail"),
    # faq
    path("faq_list/", help_views.faq_list, name="faq_list"),
    path("faq_new/", help_views.faq_new, name="faq_new"),
    path("consultant/", consultant_views.index, name="consultant_index"),
    path("help/", help_views.notice_list, name="notice_list"),
    # 질의응답
    path(
        "counsel_qna_detail/<int:id>/",
        mypage_views.counsel_qna_detail,
        name="counsel_qna_detail",
    ),
    path("counsel_qna_new/", mypage_views.counsel_qna_new, name="counsel_qna_new"),
    path(
        "counsel_qna_form/<int:id>/",
        mypage_views.counsel_qna_form,
        name="counsel_qna_form",
    ),
    path(
        "counsel_qna_delete/<int:id>/", mypage_views.counsel_qna_delete, name="counsel_qna_delete"
    ),
    path("counsel_request/", mypage_views.counsel_request, name="counsel_request"),
    path("counsel_file/", mypage_views.counsel_file, name="counsel_file"),
    path("counsel_file_delete/", mypage_views.counsel_file_delete, name="counsel_file_delete"),
    path("counsel_list/", mypage_views.counsel_list, name="counsel_list"),
    path("counsel_result/<int:id>", mypage_views.counsel_result, name="counsel_result"),
    path("counsel_qna_list/", mypage_views.counsel_qna_list, name="counsel_qna_list"),
    path("counsel_info/", mypage_views.counsel_info, name="counsel_info"),
    path("billing_cancel/", mypage_views.billing_cancel, name="billing_cancel"),
    path("mypage/upload_file/", mypage_views.upload_file, name="mypage_index"),
    path("enroll/", enroll_views.index, name="enroll_index"),
    path("pre_consult_request/", program_views.pre_consult_request, name="pre_consult_request"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
