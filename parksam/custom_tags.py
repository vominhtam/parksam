import os
import json

from django import template
import logging
from django.forms.models import model_to_dict
from traceback import format_exc
from common.models import AttachFile
from django.core.exceptions import ObjectDoesNotExist
from parksam.settings import BASE_DIR
from common.views import get_group_name

logger = logging.getLogger(__name__)

register = template.Library()


@register.simple_tag
def test_tag():
    return "test tag"


@register.simple_tag
def url_replace(request,field, value):
    d = request.GET.copy()
    d[field] = value
    return d.urlencode()


# 설문 1번 문항을 순서대로 정렬 및 idx 조건 맞추기위한 함수
@register.filter
def survey_dict_to_list(data):
    data_list = list()
    idx = 0
    for key, val in sorted(data.iteritems()):
        data_list.append([idx, key, val["cnt"], val["per"]])
        idx += 1
    return data_list


@register.filter
def survey_point_render(num):
    idx_list = list()
    for i in range(1, num + 1):
        idx_list.append(i)
    return idx_list


@register.inclusion_tag("common/dropzone.html")
def file_upload(_id, file_id=None, ext=None, group_name=None):
    ctx = {
        "id": _id,
        "multiple": False,
        "ext": ext,
        "file": "",
        "files": "",
        "group_name": group_name,
    }

    if file_id:
        try:
            f = AttachFile.objects.filter(id=file_id, use_yn="Y")
        except ObjectDoesNotExist as e:
            format_exc(e)
            f = None

        if f:
            ctx.update({"file": json.dumps(model_to_dict(f[0]))})

    # logger.debug(ctx)

    return ctx


@register.inclusion_tag("common/dropzone.html", takes_context=True)
def multi_upload(
    context, _id, group_id=None, ext=None, group_name=None, regist_id=None
):
    ctx = {
        "id": _id,
        "multiple": True,
        "ext": ext,
        "file": "",
        "files": "",
        "group_name": group_name,
    }

    if group_id:
        request = context["request"]
        if not group_name:
            group_name = get_group_name(request.path)

        logger.info("multi_upload group_id [%s]" % group_id)
        logger.info("multi_upload ext [%s]" % ext)
        logger.info("multi_upload group_name [%s]" % group_name)

        _filter = {"group_name": group_name, "group_id": group_id, "use_yn": True}

        if regist_id:
            _filter.update(regist_id=regist_id)

        g = AttachFile.objects.filter(**_filter)

        ctx.update(
            {
                "files": json.dumps([model_to_dict(f) for f in g]),
                "group_name": group_name,
            }
        )

    return ctx


@register.inclusion_tag("common/download.html")
def file_download(id):
    ctx = {"id": id, "real_name": None}

    try:
        file = AttachFile.objects.get(pk=id)
    except ObjectDoesNotExist as e:
        format_exc(e)
        return ctx

    check_path = BASE_DIR + file.save_path

    if check_path:
        ctx.update({"id": id, "real_name": file.real_name})

    return ctx


@register.inclusion_tag("common/downloads.html", takes_context=True)
def multi_download(context, group_id, zip=False, group_name=None, regist_id=None):
    file_list = list()
    _filter = dict()

    request = context["request"]
    if not group_name:
        group_name = get_group_name(request.path)

    if regist_id:
        _filter.update(regist_id=regist_id)

    files = AttachFile.objects.filter(
        group_name=group_name, group_id=group_id, use_yn="Y"
    ).filter(**_filter)

    for file in files:
        check_path = BASE_DIR + file.save_path

        if os.path.exists(check_path):
            file_list.append(file)

    ctx = {
        "zip": zip,
        "group_id": group_id,
        "files": file_list,
        "group_name": group_name,
    }

    return ctx


@register.inclusion_tag("common/preview.html")
def file_preview(id):
    try:
        if not id:
            return {}
        file = AttachFile.objects.get(pk=id)
    except ObjectDoesNotExist as e:
        format_exc(e)
        return {}

    check_path = BASE_DIR + file.save_path

    if check_path:
        ctx = {"id": id, "real_name": file.real_name, "file": file}
    else:
        ctx = {}

    return ctx


@register.inclusion_tag("common/previews.html", takes_context=True)
def multi_preview(context, group_id, zip=False, group_name=None, regist_id=None):
    file_list = list()

    request = context["request"]

    if not group_name:
        group_name = get_group_name(request.path)

    _filter = {"group_name": group_name, "group_id": group_id, "use_yn": True}

    if regist_id:
        _filter.update(regist_id=regist_id)

    files = AttachFile.objects.filter(**_filter)

    for file in files:
        check_path = BASE_DIR + file.save_path

        if os.path.exists(check_path):
            file_list.append(file)

    ctx = {
        "zip": zip,
        "group_id": group_id,
        "files": file_list,
        # 기관 공지 게시판 파일 다운로드 기능때문에 추가
        "link_yn": True if group_name in ["/board/W", "/board/S"] else False,
    }

    return ctx


@register.filter
def split(string, sep):
    """Return the string split by sep.
    Example usage: {{ value|split:"/" }}
    """
    return string.split(sep)


@register.filter
def index(list, value):
    return list.index(value)


@register.filter
def get(list, index):
    return list[index]


@register.filter()
def int(value):
    return int(value)


@register.filter(name='times') 
def times(number):
    return range(number)    
