# Create your views here.
from django.shortcuts import render, redirect


# 인덱스 화면
def index(request):

    # path = request.path

    # if path != "/":
    #     return redirect('/')

    context = {}
    return render(request, "main/index.html", context)
