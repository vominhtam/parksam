from django import forms
from .models import *
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget



class CounselForm(forms.ModelForm):
    # gender = forms.ChoiceField(choices=gender_choices, widget=forms.RadioSelect(attrs={"class":"form-control"}), initial=True)

    # counsel_date = forms.DateField(
    #     input_formats=["%m/%d/%Y"],
    #     widget=forms.DateInput(
    #         attrs={
    #             "class": "form-control",
    #         },
    #     ),
    #     help_text="Enter a date between now and 4 weeks (default 3).",
    # )

    phone1 = forms.CharField(label="핸드폰 번호1", required=True)
    phone2 = forms.CharField(label="핸드폰 번호2", required=True)
    phone3 = forms.CharField(label="핸드폰 번호3", required=True)

    student_phone = forms.CharField(required=False)

    # use_yn_choices = [(True, '사용'), (False, '미사용')]
    # title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    # # use_yn = forms.BooleanField(initial=True)
    # use_yn = forms.ChoiceField(choices=use_yn_choices, widget=forms.RadioSelect, initial=True)

    class Meta:
        model = Counsel
        fields = "__all__"

    ## 부트 스트랩 적용
    # def __init__(self, *args, **kwargs):
    #     super(CounselForm, self).__init__(*args, **kwargs)
    #     for field in iter(self.fields):
    #         self.fields[field].widget.attrs.update({
    #             'class': 'form-control'
    #         })


class CounselQnaForm(forms.ModelForm):

    class Meta:
        model = CounselQna
        fields = "__all__"
        widgets = {
            "content": SummernoteWidget(),
        }
