import json
from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse
from common.models import AttachFile
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.conf import settings
from mypage.forms import CounselForm, CounselQnaForm
from django.db import connections
from mypage.models import *
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
from member.models import UserAddinfo, UserTeacher
from common.views import sms_send
import logging

log = logging.getLogger(__name__)


# 상담 신청
@login_required
def counsel_request(request):
    user_id = request.user.id

    # validate
    if request.is_ajax():
        form = CounselForm(request.POST)

        if form.is_valid():
            obj = form.save(commit=False)
            obj.student_phone = "%s-%s-%s" % (
                form.cleaned_data.get("phone1"),
                form.cleaned_data.get("phone2"),
                form.cleaned_data.get("phone3"),
            )
            obj.regist_id = user_id
            obj.save()

            useraddinfo = UserAddinfo.objects.filter(user_id=request.user.id).first()
            userteacher = UserTeacher.objects.filter(user_id=request.user.id).first()

            user_name = ""
            user_phone = ""

            if useraddinfo:
                user_name = useraddinfo.name
                user_phone = useraddinfo.phone
            elif userteacher:
                user_name = userteacher.name
                user_phone = userteacher.phone

            # 상담 신청시 대표님, 서영님께 SMS 발송
            sms_subject = "[상담 신청이 등록 되었습니다.]"
            sms_message = """
{user_name} 님의 상담신청이 등록 되었습니다.
고객 핸드폰: {user_phone}
희망 상담일시: {counsel_date} {counsel_time}
            """.format(
                user_name=user_name,
                user_phone=user_phone,
                counsel_date=obj.counsel_date,
                counsel_time=obj.counsel_time,
            )
            log.debug("message [%s]" % sms_message)

            # 이기정
            sms_send(phone="01089294224", subject=sms_subject, message=sms_message)

            # 정서영
            sms_send(phone="01099604175", subject=sms_subject, message=sms_message)

            return JsonResponse({"success": True})
        else:
            return JsonResponse({"success": False, "message": form.errors})

    is_paid = BillingHistory.objects.filter(use_yn="Y", user=request.user).exists()

    form = CounselForm()
    context = {"form": form, "is_paid": is_paid}

    return render(request, "mypage/counsel_request.html", context)


# 상담 서류
@login_required
def counsel_file(request):
    if request.is_ajax():
        user_id = request.user.id
        files = request.POST.getlist("files")

        print("counsel_file files size:", len(files))

        for f in files:
            # 기 등록된 파일인 경우 스킵
            check_cnt = CounselFile.objects.filter(file=AttachFile.objects.get(pk=f)).count()
            if check_cnt > 0:
                CounselFile.objects.filter(file=AttachFile.objects.get(pk=f)).update(use_yn="Y")

            else:

                CounselFile.objects.create(
                    user=request.user,
                    file=AttachFile.objects.get(pk=f),
                    regist_id=request.user.id,
                )

        user_name = UserAddinfo.objects.get(user_id=request.user.id).name
        counsel_result = CounselResult.objects.filter(use_yn="Y", regist_id=user_id).order_by("-id").first()

        if counsel_result:
            teacher_id = counsel_result.teacher_id
            schedule_date = counsel_result.schedule_date
            schedule_time = counsel_result.schedule_time

            if teacher_id:
                teacher = UserTeacher.objects.get(user_id=teacher_id)
                teacher_phone = teacher.teacher_phone
                sms_subject = "[박샘, 상담서류 등록안내]"
                sms_message = """"{counsel_date} {counsel_time} 상담예정인 {user_name} 학생의 상담서류가 등록되었습니다.\n\n미리 확인 후 상담을 준비하여 주세요.""".format(
                    schedule_date=schedule_date,
                    schedule_time=schedule_time,
                    user_name=user_name,
                )
                log.debug("send qna sms to teacher, message [%s]" % sms_message)

                sms_send(phone=teacher_phone, subject=sms_subject, message=sms_message)

        return JsonResponse({"success": True})

    file_ids = CounselFile.objects.filter(user=request.user, use_yn="Y").values_list("file_id", flat=True).distinct()
    files = AttachFile.objects.filter(id__in=file_ids)
    context = {"files": json.dumps([model_to_dict(f) for f in files])}
    return render(request, "mypage/counsel_file.html", context)


@login_required
def counsel_file_delete(request):
    file_id = request.POST.get("file_id")
    print("counsel_file_delete [%s]" % file_id)
    CounselFile.objects.filter(file__id=file_id).update(use_yn="N")
    AttachFile.objects.filter(id=file_id).update(use_yn="N")

    return JsonResponse({"success": True})


# 상담 내역 리스트
@login_required
def counsel_list(request):
    user_id = request.user.id

    page = request.GET.get("page", 1)
    all_list = CounselResult.objects.filter(counsel_total__isnull=False, counsel__regist_id=user_id).extra(
        select={
            "teacher_name": "select name from auth_user_teacher a where counsel_result.teacher_id = a.user_id",
        }
    )

    print(all_list.query)

    paginator = Paginator(all_list, settings.PAGE_SIZE)
    page_list = paginator.get_page(page)
    context = {"total": all_list.count(), "page_list": page_list}

    return render(request, "mypage/counsel_list.html", context)


# 상담 내역 리스트
@login_required
def counsel_result(request, id):
    counsel_result = (
        CounselResult.objects.filter(id=id)
        .extra(
            select={"teacher_name": "select name from auth_user_teacher a where counsel_result.teacher_id = a.user_id"}
        )
        .first()
    )

    # files 는 group_name : counsel_resutl, group_id = counsel_result.id 로 조회
    file_id_list = CounselResultFile.objects.filter(counsel_result=counsel_result, use_yn="Y").values_list(
        "file__id", flat=True
    )
    files = AttachFile.objects.filter(id__in=file_id_list)

    context = {"counsel_result": counsel_result, "files": files}

    return render(request, "mypage/counsel_result.html", context)


# 질의 응답
@login_required
def counsel_qna_list(request):
    page = request.GET.get("page", 1)
    user_id = request.user.id
    question_list = (
        CounselQna.objects.filter(use_yn="Y", regist_id=user_id)
        .extra(
            select={
                "name": "select name from auth_user_addinfo where auth_user_addinfo.user_id = counsel_qna.regist_id"
            }
        )
        .order_by("-id")
    )
    paginator = Paginator(question_list, settings.PAGE_SIZE)
    page_list = paginator.get_page(page)
    is_paid = BillingHistory.objects.filter(use_yn="Y", user=request.user).exists()
    print("is_paid:", is_paid)
    context = {"page_list": page_list, "is_paid": is_paid}
    return render(request, "mypage/counsel_qna_list.html", context)


@login_required
def counsel_qna_detail(request, id):
    counsel_qna = (
        CounselQna.objects.filter(id=id)
        .extra(
            select={
                "teacher_name": "select name from counsel_qna_answer a, auth_user_teacher b where a.regist_id = b.user_id and a.counsel_qna_id = counsel_qna.id",
                "teacher_answer": "select content from counsel_qna_answer where counsel_qna_answer.counsel_qna_id = counsel_qna.id",
            }
        )
        .first()
    )
    # form = CounselQnaForm(instance=news)
    files1 = AttachFile.objects.filter(use_yn="Y", group_name="counsel_qna_user", group_id=id)
    files2 = AttachFile.objects.filter(use_yn="Y", group_name="counsel_qna", group_id=id)
    context = {"counsel_qna": counsel_qna, "files1": files1, "files2": files2}

    return render(request, "mypage/counsel_qna_detail.html", context)


@login_required
def counsel_qna_form(request, id):
    user_id = request.user.id
    qna = CounselQna.objects.get(id=id)

    print("counsel_qna_form id [%s]" % id)

    if request.is_ajax():
        form = CounselQnaForm(request.POST, instance=qna)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.regist_id = user_id
            obj.modify_id = user_id
            obj.save()

            files = request.POST.getlist("files")
            if files:
                AttachFile.objects.filter(id__in=files).update(group_name="counsel_qna_user", group_id=obj.pk)

            return JsonResponse({"success": True})
        else:
            return JsonResponse({"success": False, "message": form.errors})

    form = CounselQnaForm(instance=qna)
    files = AttachFile.objects.filter(use_yn="Y", group_name="counsel_qna_user", group_id=id)

    context = {"form": form, "files": json.dumps([model_to_dict(f) for f in files])}
    return render(request, "mypage/counsel_qna_form.html", context)


# 이용 정보
@login_required
def counsel_info(request):
    page = request.GET.get("page", 1)
    all_list = (
        BillingHistory.objects.filter(user=request.user)
        .extra(
            select={
                "next_date": "adddate(adddate(regist_date, interval 1 month), interval -1 day)",
                "schdule_date": "select max(str_to_date(schedule_date, '%%Y/%%m/%%d')) from counsel a, counsel_result b where b.is_counseled = 'N' and  a.id = b.counsel_id and a.regist_id = billing_history.regist_id",
            }
        )
        .order_by("-regist_date")
    )
    paginator = Paginator(all_list, settings.PAGE_SIZE)
    page_list = paginator.get_page(page)
    context = {"total": all_list.count(), "page_list": page_list}

    return render(request, "mypage/counsel_info.html", context)


# 이용 정보
@login_required
def billing_cancel(request):
    try:
        id = request.POST.get("id")
        BillingHistory.objects.filter(id=id).update(use_yn="N")
        return JsonResponse({"success": True})
    except:
        return JsonResponse({"success": False})


@require_POST
def upload_file(request):
    user_id = request.user.id

    print("user_id:", user_id)

    fs = FileSystemStorage()

    if "file1" in request.FILES:
        myfile1 = request.FILES["file1"]
        file_name1 = fs.save(myfile1.name, myfile1)
        file_url1 = fs.url(file_name1)
    else:
        file_name1 = ""

    if "file2" in request.FILES:
        myfile2 = request.FILES["file2"]
        file_name2 = fs.save(myfile2.name, myfile2)
        file_url2 = fs.url(file_name2)
    else:
        file_name2 = ""

    context = {
        "file_name1": file_name1,
        "file_name2": file_name2,
    }

    return render(request, "mypage/index.html", context)


# 질의응답
@login_required
def counsel_qna_new(request):

    user_id = request.user.id

    if request.is_ajax():
        form = CounselQnaForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.regist_id = user_id
            obj.save()

            files = request.POST.getlist("files")
            if files:
                AttachFile.objects.filter(id__in=files).update(group_name="counsel_qna_user", group_id=obj.pk)

            # 담당 컨설턴트가 있을경우 sms 발송
            user = request.user
            addinfo = UserAddinfo.objects.get(user_id=user)
            user_name = addinfo.name
            counsel_result = CounselResult.objects.filter(use_yn="Y", regist_id=user_id).order_by("-id").first()

            if counsel_result:
                teacher_id = counsel_result.teacher_id

                if teacher_id:
                    teacher = UserTeacher.objects.get(user_id=teacher_id)
                    teacher_phone = teacher.teacher_phone
                    sms_subject = "[박샘, 일대일 질문 등록]"
                    sms_message = """{user_name} 회원님의 질문이 등록되었습니다. 확인 후 답변을 등록해 주세요.""".format(user_name=user_name)
                    log.debug("send qna sms to teacher, message [%s]" % sms_message)

                    sms_send(phone=teacher_phone, subject=sms_subject, message=sms_message)

            return JsonResponse({"success": True})
        else:
            return JsonResponse({"success": False, "message": form.errors})

    form = CounselQnaForm()
    context = {"form": form}

    return render(request, "mypage/counsel_qna_new.html", context)


def counsel_qna_delete(request, id):
    user_id = request.user.id
    CounselQna.objects.filter(id=id).update(use_yn="N", modify_id=user_id)
    return JsonResponse({"success": True})