from django.db import models
from django.contrib.auth.models import User
from common.models import AttachFile

# Create your models here.


class Counsel(models.Model):
    counsel_date = models.CharField(max_length=10, verbose_name="희망 상담일자")
    counsel_time = models.CharField(max_length=5, verbose_name="희망 상담시간")
    student_name = models.CharField(max_length=100, verbose_name="학생 이름")
    student_gender = models.CharField(max_length=10, verbose_name="학생 성별")
    student_phone = models.CharField(max_length=13, verbose_name="학생 핸드폰 번호")
    grade = models.CharField(max_length=10, verbose_name="학년")
    school = models.CharField(max_length=100, verbose_name="학교")
    direction = models.CharField(max_length=20, verbose_name="계열")

    level = models.IntegerField(verbose_name="내신등급")
    hope_school1 = models.CharField(max_length=100, verbose_name="희망 대학/학과1")
    hope_school2 = models.CharField(max_length=100, blank=True, null=True, verbose_name="희망 대학/학과2")
    hope_school3 = models.CharField(max_length=100, blank=True, null=True, verbose_name="희망 대학/학과3")

    counsel_subject = models.CharField(max_length=100, verbose_name="상담 희망분야")
    counsel_request_memo = models.TextField(blank=True, null=True, verbose_name="상담 희망분야")
    counsel_join = models.CharField(max_length=100, verbose_name="참석자")
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")
    regist_id = models.IntegerField(db_column="regist_id", blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(db_column="modify_id", blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = "counsel"


class CounselFile(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, db_column="user_id", verbose_name="내부 아이디")
    file = models.ForeignKey(AttachFile, on_delete=models.CASCADE, db_column="file_id", verbose_name="파일 아이디")
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")

    regist_id = models.IntegerField(db_column="regist_id", blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(db_column="modify_id", blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = "counsel_file"


class CounselTeacher(models.Model):
    counsel = models.ForeignKey(Counsel, on_delete=models.CASCADE, db_column="counsel_id", verbose_name="상담 아이디")
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, db_column="teacher_id", verbose_name="선생님 아이디")

    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")

    regist_id = models.IntegerField(db_column="regist_id", blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(db_column="modify_id", blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = "counsel_teacher"


class CounselResult(models.Model):
    counsel = models.ForeignKey(Counsel, on_delete=models.CASCADE, db_column="counsel_id", verbose_name="상담 아이디")

    counsel_type = models.CharField(max_length=10, blank=True, null=True)

    # 관리자가 전화 상담후 일정을 지정
    schedule_date = models.CharField(max_length=10, blank=True, verbose_name="예정 상담일자")
    schedule_time = models.CharField(max_length=5, blank=True, verbose_name="예정 상담시간")

    is_called = models.CharField(max_length=1, default="N")
    is_counseled = models.CharField(max_length=1, default="N")

    student_name = models.CharField(max_length=100, verbose_name="학생 이름")
    teacher = models.ForeignKey(User, on_delete=models.CASCADE, db_column="teacher_id", verbose_name="선생님 아이디")

    zoom_link = models.CharField(max_length=100, blank=True, verbose_name="줌 링크")

    counsel_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    counsel_total = models.TextField(blank=True)
    counsel_after = models.TextField(blank=True)
    next_counsel_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)

    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")

    regist_id = models.IntegerField(db_column="regist_id", blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(db_column="modify_id", blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = "counsel_result"


class CounselResultFile(models.Model):
    counsel = models.ForeignKey(Counsel, on_delete=models.CASCADE, db_column="counsel_id", verbose_name="상담 아이디")
    counsel_result = models.ForeignKey(CounselResult, on_delete=models.CASCADE, db_column="counsel_result_id", verbose_name="상담결과 아이디")
    file = models.ForeignKey(AttachFile, on_delete=models.CASCADE, db_column="file_id", verbose_name="파일 아이디")
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")

    regist_id = models.IntegerField(db_column="regist_id", blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(db_column="modify_id", blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = "counsel_result_file"


# 질의응답
class CounselQna(models.Model):
    title = models.CharField(max_length=100, blank=False)
    content = models.TextField(blank=False)
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")
    regist_id = models.IntegerField(blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True, auto_now=True)

    class Meta:
        db_table = "counsel_qna"


# 질의응답 답변
class CounselQnaAnswer(models.Model):
    counsel_qna = models.ForeignKey(
        CounselQna, on_delete=models.CASCADE, db_column="counsel_qna_id", verbose_name="질의응답 번호"
    )
    content = models.TextField(blank=False)
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")
    regist_id = models.IntegerField(blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True, auto_now=True)

    class Meta:
        db_table = "counsel_qna_answer"


"""
결제 결과 이력을 저장하는 모델 생성

- 결제 취소 또는 사용 완료시에는 use_yn 을 False 로 변경
- modify_id 값을 결제 취소는 사용자, 사용완료시에는 SYSTEM 으로 지정하여 변경
"""


class BillingHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, db_column="user_id")
    billing_type = models.CharField(max_length=10)  # 결제 유형에 따라, regular 또는 onetime 형태로 저장
    regular_type = models.CharField(max_length=1, blank=True)
    cost = models.IntegerField(blank=False, null=False)
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")
    regist_id = models.IntegerField(blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True, auto_now=True)

    class Meta:
        db_table = "billing_history"
