from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from data.models import BoardNotice, BoardFaq
from data.forms import BoardNoticeForm
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.core.paginator import Paginator
from django.conf import settings
from mypage.models import BillingHistory


@login_required
def notice_new(request):

    user = request.user
    if not user.is_staff:
        # 접근권한이 없음을 안내하고 index 로 보내도록 기능 추가
        return redirect("/")

    if request.is_ajax():
        form = BoardNoticeForm(request.POST, initial={"order_no": 1, "fix_yn": False, "use_yn": True})
        if form.is_valid():
            form.save()
            return JsonResponse({"success": True})
        else:
            return JsonResponse({"success": False, "message": form.errors})

    form = BoardNoticeForm()
    context = {"form": form}

    return render(request, "help/notice_new.html", context)


def notice_list(request):
    id = request.GET.get("id")
    page = request.GET.get("page", 1)
    notice_all_list = BoardNotice.objects.filter(use_yn="Y").order_by("-id")
    paginator = Paginator(notice_all_list, settings.PAGE_SIZE)
    page_list = paginator.get_page(page)
    context = {"page_list": page_list, "id": id}
    return render(request, "help/notice_list.html", context)


def notice_detail(request, id):
    notice = BoardNotice.objects.get(id=id)
    form = BoardNoticeForm(instance=notice)
    context = {"form": form, "notice": notice}
    return render(request, "help/notice_detail.html", context)


def faq_list(request):
    faq_list = BoardFaq.objects.filter(use_yn="Y").order_by("order_no", "id")

    context = {"faq_list": faq_list}
    return render(request, "help/faq_list.html", context)


def faq_new(request):
    page = request.GET.get("page", 1)
    faq_all_list = BoardFaq.objects.filter(use_yn="Y").order_by("-id")
    context = {"faq_list": faq_all_list}
    return render(request, "help/faq_list.html", context)
