import requests
import json
import datetime
import traceback
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.db import connections
from django.views.decorators.csrf import csrf_exempt
from mypage.models import BillingHistory
from program.models import PreConsult


def index(request):
    return render(request, "program/index.html")


def member_counseltant(request):
    return render(request, "program/member_counseltant.html")


def member_counseltant_lite(request):
    return render(request, "program/member_counseltant_lite.html")


@csrf_exempt
@login_required
def regular_payment(request):
    is_paid = BillingHistory.objects.filter(
        billing_type="regular", use_yn="Y", user=request.user
    ).exists()
    today = datetime.datetime.now().strftime("%Y.%m.%d")

    # 결제한 정보가 있다면 알림 후 마이페이지로 보낸다
    # 결제 정보 확인 후 21.6.8일 이전의 사용자는 48500원, 그외에는 49500으로 이용
    # first_regist_date = BillingHistory.objects.filter(user=request.user).aggregate(Min('regist_date'))['regist_date__min']
    with connections["default"].cursor() as cur:
        query = """
            SELECT 
                CASE
                    WHEN
                        first_regist_date < DATE('21/6/8')
                            AND NOW() < DATE('22/6/7')
                    THEN
                        '48500'
                    ELSE '49500'
                END cost
            FROM
                (SELECT 
                    a.id,
                        a.username,
                        a.email,
                        b.name,
                        b.phone,
                        MIN(c.regist_date) first_regist_date
                FROM
                    auth_user a, auth_user_addinfo b, billing_history c
                WHERE
                    a.id = b.user_id AND a.id = c.regist_id
                        AND c.billing_type = 'regular'
                        AND a.id = {user_id}
                GROUP BY a.id , a.username , a.email , b.name , b.phone) t1
        """.format(
            user_id=request.user.id
        )

        print(query)
        cur.execute(query)
        row = cur.fetchone()

    if row:
        cost = row[0]
    else:
        cost = "49500"

    context = {"today": today, "is_paid": is_paid, "cost": cost}
    return render(request, "program/regular_payment.html", context)


@csrf_exempt
@login_required
def lite_payment(request):
    is_paid = BillingHistory.objects.filter(
        billing_type="lite", use_yn="Y", user=request.user
    ).exists()
    today = datetime.datetime.now().strftime("%Y.%m.%d")
    cost = "33000"

    context = {"today": today, "is_paid": is_paid, "cost": cost}
    return render(request, "program/lite_payment.html", context)


def non_member_counseltant(request):
    return render(request, "program/non_member_counseltant.html")


@csrf_exempt
@login_required
def onetime_payment(request):
    today = datetime.datetime.now().strftime("%Y.%m.%d")
    context = {"today": today}

    return render(request, "program/onetime_payment.html", context)


@csrf_exempt
@login_required
def test_billings(request):
    try:
        print("test_billings called")

        cost = request.POST.get("cost")
        customer_uid = request.POST.get("customer_uid")

        print("cost:", cost)
        print("customer_uid::::::::::::::::::::::::::::::::::::", customer_uid)

        # access_token 생성
        headers = {"Content-Type": "application/json"}
        data = {
            "imp_key": "8353848662751605",
            "imp_secret": "f703358107c92ec7c00e61b2638b19ee910a9793d04e1ef7abc04ae7037bdda3e2207fd70b5bbc65",
        }
        response = requests.post(
            "https://api.iamport.kr/users/getToken",
            headers=headers,
            data=json.dumps(data),
            verify=False,
        )
        access_token = json.loads(response.text)["response"]["access_token"]

        print("access_token:", access_token)

        # 회원의 결제 번호를 생성.
        merchant_uid = "payment_r_%s_%s" % (
            request.user.id,
            datetime.datetime.now().strftime("%Y%m%d%H%M%S"),
        )

        print("merchant_uid:", merchant_uid)

        from time import time

        unix_time = int(time())

        # 5초후 결제 예약

        unix_time = unix_time + 5

        headers = {"Content-Type": "application/json", "Authorization": access_token}

        d = {
            "customer_uid": customer_uid,
            "schedules": [
                {
                    "merchant_uid": merchant_uid,
                    "schedule_at": unix_time,
                    "amount": 100,
                    "name": "월간 이용권 정기결제 테스트",
                    "buyer_name": "이종호",
                    "buyer_tel": "01032348559",
                    "buyer_email": "redukyo@gmail.com",
                }
            ],
        }

        # 결제 요청
        response = requests.post(
            "https://api.iamport.kr/subscribe/payments/schedule",
            headers=headers,
            data=json.dumps(d),
            verify=False,
        )

        print("payment result:", response)

        result = json.loads(response.text)

        print("result:", result)

        return JsonResponse({"success": True, "message": result}, safe=False)
    except Exception as e:
        print("Exception!!!!!!!!!!!!!!!!!!!!!!!!")
        print(traceback.format_exc())
        return JsonResponse({"success": False, "message": traceback.format_exc()}, safe=False)


@csrf_exempt
@login_required
def billings(request):
    try:
        print("billings called")

        cost = request.POST.get("cost")
        customer_uid = request.POST.get("customer_uid")

        print("cost:", cost)
        print("customer_uid::::::::::::::::::::::::::::::::::::", customer_uid)

        # access_token 생성
        headers = {"Content-Type": "application/json"}
        data = {
            "imp_key": "8353848662751605",
            "imp_secret": "f703358107c92ec7c00e61b2638b19ee910a9793d04e1ef7abc04ae7037bdda3e2207fd70b5bbc65",
        }
        response = requests.post(
            "https://api.iamport.kr/users/getToken",
            headers=headers,
            data=json.dumps(data),
            verify=False,
        )
        access_token = json.loads(response.text)["response"]["access_token"]

        print("access_token:", access_token)

        # 회원의 결제 번호를 생성.
        merchant_uid = "payment_r_%s_%s" % (
            request.user.id,
            datetime.datetime.now().strftime("%Y%m%d%H%M%S"),
        )

        print("merchant_uid:", merchant_uid)

        headers = {"Content-Type": "application/json", "Authorization": access_token}
        data = {
            "customer_uid": customer_uid,
            "merchant_uid": merchant_uid,  # 새로 생성한 결제(재결제)용 주문 번호
            "amount": cost,
            "name": "월간 이용권 정기결제",
        }

        # 결제 요청
        response = requests.post(
            "https://api.iamport.kr/subscribe/payments/again",
            headers=headers,
            data=json.dumps(data),
            verify=False,
        )

        print("payment result:", response)

        result = json.loads(response.text)

        print("result:", result)

        return JsonResponse({"success": True, "message": result}, safe=False)
    except Exception as e:
        print("Exception!!!!!!!!!!!!!!!!!!!!!!!!")
        print(traceback.format_exc())
        return JsonResponse({"success": False, "message": traceback.format_exc()}, safe=False)


# 결제 결과 이력 저장
# 해당 내용으로 다음 정기결제를 주기적으로 진행
@login_required
def save_billing_result(request):
    user = request.user
    amount = request.POST.get("amount")

    try:
        if int(amount) < 100000:
            billing_type = "regular"
        else:
            billing_type = "onetime"

        BillingHistory.objects.create(
            user=user, billing_type=billing_type, cost=amount, use_yn="Y", regist_id=user.id
        )

        # save logic
        return JsonResponse({"success": True})
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"success": False, "message": traceback.format_exc()})


def pre_consult_request(request):
    if request.is_ajax():
        body = request.body.decode("utf-8")
        if "&" in body:
            body = body.split("&")[0]

        data = json.loads(body)

        try:
            PreConsult.objects.create(**data)
            return JsonResponse({"success": True})
        except:
            pass

    return JsonResponse({"success": False})
