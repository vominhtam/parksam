from django.db import models
from django.contrib.auth.models import User


class PreConsult(models.Model):
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    date = models.DateTimeField()

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "pre_consult"