/* Add here all your JS customizations */
let is_loading = false;
let listScrollTop = 0;
let detailScrollTop = 0;

// 브라우저의 뒤로가기 이벤트시 처리
$(window).bind("pageshow", function (event) {
    if (event.persisted || (window.performance && window.performance.navigation.type == 2)) {
        //console.debug('BFCahe 로부터 복원됨');
        //search();
    }
});

$(document).ready(function () {

    $("#form *")
            .focus(function () {
                $(this).removeClass("is-invalid").removeAttr('data-error-message');
            })
            .blur(function () {
                $(this).parent().find("span.is-invalid").remove();
            });


    // esc 키 입력시 기본 호출 이벤트
    $(this).keyup(function (e) {
        if (e.keyCode === 27) {
            goBack(e);
        }
    });

    setDatePicker();
    setNotKor();

    $(".form-group input").keyup(function (e) {
        //console.log('e.keyCode:' + e.keyCode);
        if (e.keyCode == 13) {
            search();
        }
    });

    $.ajaxSetup({

        beforeSend: function (xhr, settings) {
            if(this.data){            
                this.data += '&' + $.param({
                    csrfmiddlewaretoken: $.cookie("csrftoken")
                });
            }else{
                this.data = $.param({csrfmiddlewaretoken: $.cookie("csrftoken")});

            }
            console.log('beforeSend param check --- s');
            console.log(this.data);
            console.log('beforeSend param check --- e');
            $("#block_layer").stop().fadeIn(600);

            // alert('abort');
            // xhr.abort(); //abort all current request here
            /*
            $.ajax({
                url: '/common/sample',
                async: false
            });
            */
        },
        complete: function (data) {
            $("#block_layer").stop().fadeOut(200);

            if (is_loading) {
                //console.log('ajax done');
                is_loading = false;
            }
        },
        error: function (xhr, status, err) {
            // alert(err);
            $("#block_layer").stop().fadeOut(100);
        }
    });
});

function setNotKor() {
    $(".not-kor").on("keyup", function (e) {
        if (!(e.keyCode >= 37 && e.keyCode <= 40)) {
            let v = $(this).val();
            $(this).val(v.replace(/[^a-z0-9_-][\.]/gi, ''));
        }
    });
}

/*
$(window).on("beforeunload", function () {
    // 상세, 수정 화면 표시중 화면이동은 경고창을 표시.
    // 개별 메세지는 보안상 이슈로 표시할 수 없음. https://stackoverflow.com/questions/37782104/javascript-onbeforeunload-not-showing-custom-message
    if ($("#detail-view, #form-view").size() > 0) {
        return "check";
    }

});
*/


let timeOut;
let isIng = false;
let isDone = false;

function set_dropzone(id, multiple, file, files, ext, group_name) {

    Dropzone.autoDiscover = false;
    Dropzone.prototype.defaultOptions.dictDefaultMessage = "업로드 하고싶은 파일을 드랍하거나 클릭하여 등록하세요.";
    Dropzone.prototype.defaultOptions.dictFallbackMessage = "브라우저가 파일 업로드를 지원하지 않습니다.";
    Dropzone.prototype.defaultOptions.dictFallbackText = "Please use the fallback form below to upload your files like in the olden days.";
    Dropzone.prototype.defaultOptions.dictFileTooBig = "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.";
    Dropzone.prototype.defaultOptions.dictInvalidFileType = "해당 형식의 파일은 업로드 할 수 없습니다.";
    Dropzone.prototype.defaultOptions.dictResponseError = "Server responded with {{statusCode}} code.";
    Dropzone.prototype.defaultOptions.dictCancelUpload = "업로드 취소";
    Dropzone.prototype.defaultOptions.dictUploadCanceled = "업로드 취소 완료";
    Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "Are you sure you want to cancel this upload?";
    Dropzone.prototype.defaultOptions.dictRemoveFile = "<button type='button' class='btn btn-warning btn-sm'>삭제</button>";
    Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "You can not upload any more files.";

    // console.debug('set dropzone id: ' + id);

    if ($('#' + id)[0].dropzone) {
        alert(id + '객체는 이미 dropzone이 있습니다.');
    } else {
        $('#' + id).dropzone({
            url: "/common/file_upload/",
            uploadMultiple: false,
            paramName: id,
            parallelUploads: 100,
            // thumbnailWidth: 70,
            // thumbnailHeight: 70,
            addRemoveLinks: true,
            autoProcessQueue: false,
            dictRemoveFileConfirmation: '삭제하시겠습니까?',
            headers: {
                'X-CSRF-Token': $.cookie('csrftoken'),
                csrfmiddlewaretoken: $.cookie('csrftoken')
            },
            init: function () {
                if (file) {
                    file = JSON.parse(file);
                    file.name = file.real_name;
                    file.size = file.real_size;

                    this.files.push(file);

                    this.emit("addedfile", file);

                    switch (file.ext) {
                        case "csv":
                        case "xls":
                        case "xlsx":
                            console.log('thumbnail : file_image.png');
                            this.emit("thumbnail", file, '/staticfiles/image/file_image.png');
                            break;
                        default:
                            console.log('thumbnail : default');
                            this.emit("thumbnail", file, '/upload_files/' + file.save_name);
                            break;

                    }

                    this.emit("complete", file);

                    jQuery('<input/>', {
                        type: 'hidden',
                        name: id,
                        value: file.id,
                        id: file.id
                    }).appendTo('#' + id);

                } else if (files) {

                    console.debug('files -> ', files);

                    files = JSON.parse(files);

                    for (f in files) {
                        // console.debug(files[f]);
                        file = files[f];

                        file.name = file.real_name;
                        file.size = file.real_size;

                        this.files.push(file);

                        console.log('file.save_name: ' + file.save_name);

                        this.emit("addedfile", file);
                        switch (file.ext) {
                            case "csv":
                            case "xls":
                            case "xlsx":
                                console.log('thumbnail : file_image_1.png');
                                this.emit("thumbnail", file, '/staticfiles/image/file_image_s.png');
                                break;
                            default:
                                console.log('thumbnail : default');
                                this.emit("thumbnail", file, '/upload_files/' + file.save_name);
                                break;

                        }
                        this.emit("complete", file);

                        jQuery('<input/>', {
                            type: 'hidden',
                            name: id,
                            value: file.id,
                            id: file.id
                        }).appendTo(id);
                    }
                }

                this.on("sending", function (file, xhr, formData) {
                    console.log('set group_name: ' + group_name);

                    if (group_name)
                        formData.append("path", group_name);
                    else
                        formData.append("path", window.location.pathname);
                });

                this.on('success', function (file, json) {

                    var result = JSON.parse(json);
                    // 업로드된 정보를 form 에 추가
                    jQuery('<input/>', {
                        type: 'hidden',
                        name: id,
                        value: result.id,
                        'data-uuid': file.upload.uuid
                    }).appendTo('#' + id);
                });

                this.on("addedfile", function (file) {
                    if (file.name.indexOf(",") > -1) {
                        alert("파일명에 \",\"는 허용되지 않아 \"_\"로 변환되어 저장됩니다.");
                    }

                    // 허용 확장자가 있는 경우 체크
                    if (ext) {
                        ext = ext.toLowerCase();
                        let file_arr = file.name.split(".");
                        file_ext = file_arr[file_arr.length - 1].toLowerCase();
                        let regExp = new RegExp(file_ext, 'ig');

                        console.debug(regExp);

                        if (!regExp.test(ext)) {
                            swal("경고", "허용된 확장자가 아닙니다.", "warning");
                            this.removeFile(file);
                        }
                    }

                    if (multiple == "True") {

                    } else {
                        if (this.files[1] != null) {
                            this.removeFile(this.files[0]);
                        }
                    }
                });

                this.on('removedfile', function (file) {
                    if (file.id) {
                        $.post(
                            '/common/file_delete/',
                            {id: file.id},
                            function (data) {
                                if (data.status == 'success') {
                                    $("#" + file.id).remove();
                                } else {
                                    console.debug('file delete error');
                                }
                            }
                        )
                    }
                });
            }
        });
    }
}

// 상세화면을 레이어로 표현
function detail_view(url) {
    console.debug('detail_view url: ' + url);
    console.time("detail_view");
    listScrollTop = $(".content-body").scrollTop();

    if ($("#form-view"))
        $("#form-view").remove();

    if (url) {
        // $("section[role='main']>form").hide();

        $("#detail-view").remove();
        $("section[role='main']").append("<div id='detail-view' class='row detail-view'></div>");

        $.get(url, {
            csrfmiddlewaretoken: $.cookie('csrftoken')
        }, function (data) {
            // console.debug(data);
            $("#detail-view").html(data);

            // hide list and show detail
            // console.debug('section size: ' + $("section[role='main']>section").size());

            $("section[role='main']>section").hide(0, function () {
                $(".content-body").scrollTop(0);
                $("#detail-view").fadeIn(200, function () {
                    console.log('call setDatePicker');
                    setDatePicker();
                });
            });

            // 브라우저 뒤로 가기 이벤트에 대한 처리
            history.pushState(null, null, location.href);
            window.onpopstate = function (event) {
                $("section[role='main']>section").show();
                $("#detail-view").remove();
            };

        });
    }
    console.timeEnd("detail_view");
}

// 상세화면을 레이어로 표현
function form_view(url) {
    console.debug("form_view url:", url);
    console.time("form_view");
    detailScrollTop = $(".content-body").scrollTop();

    if (url) {
        $.get(url, {
            csrfmiddlewaretoken: $.cookie('csrftoken')
        }, function (data) {

            history.pushState(null, null, location.href);
            window.onpopstate = function (event) {
                $("section[role='main']>section").show();

                if ($("#detail-view").length) {
                    $.when(
                        $("#form-view").remove()
                    ).then(
                        $("#detail-view").fadeIn(100)
                    );
                } else {
                    $("#form-view").remove();
                }
            };

            $("#form-view").remove();
            $("section[role='main']").append("<div id='form-view' class='row form-view'></div>");

            $("#form-view").html(data);

            console.log('call setDatePicker !');
            setDatePicker();
            console.log('call setNotKor !');
            setNotKor();

            $("section[role='main']>.panel, section[role='main']>form, #detail-view").hide(0, function () {
                $(".content-body").scrollTop(0);
                $("#form-view").fadeIn(200);
            });
        });


    } else {
        $("#form-view").hide(0, function () {
            $("#form-view").remove();
        });
    }

    console.timeEnd("form_view");
}

function noScroll() {
    window.scrollTo(0, 0);
}

function popup_view(url) {
    console.debug("popup_view url:", url);
    console.time("popup_view");

    let windowHeight = $(window).innerHeight(),
        windowWidth = $(window).innerWidth(),
        docHeight = 0,
        docWidth = 0;

    $("#popup-bg").remove();
    $("body").append("<div id='popup-bg' class='row popup_bg'></div>");
    $("#popup-bg").append("<div id='popup-view' class='row popup_view'><img src='/staticfiles/assets/images/close-round.png' class='popup_close_img'></div>");

    setDatePicker();
    setNotKor();

    $("#popup-view").draggable({
        containment: 'parent',
        cursor: 'move'
    });

    // add listener to disable scroll
    window.addEventListener('scroll', noScroll);
    $(".popup_close_img").click(function () {
        goBack();
    });

    /*
    $("#popup-bg").dblclick(function () {
        goBack();
    });
    */

    if (url) {
        $.get(url, {
            csrfmiddlewaretoken: $.cookie('csrftoken')
        }, function (data) {
            $("#popup-view").append(data);

            $("#popup-bg").fadeIn(200).css("display", "grid");

        });


    } else {
        $("#popup-view").html("<div style='text-align: center; line-height: 100px;'><h2>표시할 내용이 없습니다.</h2></div>");
        $("#popup-bg").fadeIn(100);

        setTimeout(function () {
            if ($("#popup-bg").length) {
                toList();
            }
        }, 1000);
    }

    console.timeEnd("popup_view");
}

// 수정된 내용을 저장
// func 가 지정되면 저장처리 이후 callback 함수로 실행되도록 추가.
function save(url, func) {
    if (isDone) {
        isIng = false;
        isDone = false;

        clearTimeout(timeOut);

        // form-view 안의 form 을 순차적으로 전송
        var forms1 = $("#form-view form");
        var forms2 = $("#popup-view form");
        let forms = forms1.add(forms2);
        var forms_length = forms.length;

        if (forms_length == 0) {
            alert("저장할 폼이 없습니다.");
            return;
        }

        var idx = 0;
        var detail_url;

        $.each(forms, function (index) {
            idx++;

            var action = $(this).attr("action");

            if (action) {
                url = action;
            }

            // console.debug("[" + index + "] url = " + url);
            if (url) {
                console.debug("theme.custom.js save url: " + url);

                var data = $(this).serializeArray(); // convert form to array

                // console.debug(data);

                data.push({name: "csrfmiddlewaretoken", value: $.cookie('csrftoken')});


                // 화면이 detail 이 아닌 form 을 바로 사용하는 경우의 케이스 처리
                let view_type = 0;

                if ($("#detail-view").length)
                    view_type = 1;
                else if ($("#form-view").length)
                    view_type = 2;

                if ($("#popup-bg").length)
                    view_type = 3;

                $.post(url, data, function (data) {
                    console.debug(data);

                    if (data.success) {

                        let message = "";

                        if (data.message)
                            message = data.message;


                        // console.debug("theme.custom.js save success");
                        // 추가의 경우 detail-view 가 없으므로 확인후 detail-view 를 추가한다

                        let f = window[func];

                        if (typeof f == 'function') {
                            f();
                        }

                        $("#popup-bg").remove();

                        swal("저장 되었습니다.", message, "info").then(function () {

                            console.debug("save view_type: " + view_type);
                            console.debug("data.html --- s");
                            console.debug(data.html);
                            console.debug("data.html --- e");

                            if (data.html) {

                                $("#detail-view, #form-view").remove();

                                console.debug('data.html exists');
                                // console.debug("theme.custom.js data.html exists");
                                if (view_type == 1) {
                                    $("section[role='main']").append("<div id='detail-view' class='row detail-view'></div>");
                                    $("#detail-view").html(data.html).show();
                                } else if (view_type == 2) {
                                    $("#form-view").remove();
                                    $("section[role='main']").append("<div id='form-view' class='row form-view'></div>");
                                    $("#form-view").html(data.html).show();
                                } else {
                                    alert("view_type 이 없습니다. 시스템 관리자에게 문의 바랍니다.");
                                    return;
                                }

                            } else if (view_type == 2) {
                                callSearch();

                                $("#detail-view, #form-view").remove();

                                $("section[role='main']>section").show(0, function () {
                                    $(".content-body").scrollTop(listScrollTop);
                                });

                            } else if (view_type == 3) {
                                console.debug('view_type ====> 3 ' + $("#popup-bg").length);
                                $("#popup-bg").remove();
                                return;

                            } else if (data.url) {
                                console.debug('data.url exists');
                                detail_url = data.url;

                            } else {
                                console.debug('noting exists');
                                $("section[role='main']>form").show();
                                search();
                            }

                            // detail_url 이 존재하고, form 의 서브밋이 모두 종료되면 상세보기를 실행
                            if (detail_url && idx == forms_length) {
                                detail_view(detail_url);
                            }
                        });

                    } else {
                        console.debug("theme.custom.js save error");
                        //console.debug(data);

                        if (data.errors) {
                            let keys = [];
                            let errors = data.errors;
                            let messages = "입력되지 않은 항목이 있습니다.\n\n";

                            $.each(errors, function (key, value) {
                                $.each(value, function (index) {
                                    messages += "- " + key + ": " + value[index] + "\n";
                                });
                            });

                            swal("저장되지 않았습니다.", messages, "info");

                            $("label").removeClass("error");

                            for (let i in keys) {
                                $("label[for*='" + keys[i] + "']").addClass("error");

                                /*
                                let error_list = errors[keys[i]];
                                for (let j in error_list) {
                                    $("label[for$='" + keys[i] + "']").addClass("error");
                                    $("*[name='" + keys[i] + "']").css("border", "1px solid #B94A48").attr("title", error_list[j]);
                                }
                                */
                            }
                        } else if (data.message) {
                            swal("저장되지 않았습니다.", data.message, "error");
                        } else if (data.org) {
                            alert("참여기관이 중복되었습니다.");
                        } else
                            alert("관리자에게 문의 바랍니다..");
                    }
                });
            } else {
                alert('URL IT NOT EXISTS');
            }
        });

    } else {
        // 파일 업로드 체크
        fileUploadCheck();

        timeOut = setTimeout(function () {
            save(url, func);
        }, 500);
    }
}

function save_file(url) {
    if (isDone) {
        isIng = false;
        isDone = false;

        clearTimeout(timeOut);

        if (url) {
            console.debug("theme.custom.js save_file url: " + url);

            var data = $("#form").serializeArray(); // convert form to array

            $.post(url, data, function (data) {
                console.debug(data);

                if (data.success) {
                    /*
                    Swal.fire({
                        title: "",
                        text: "저장 되었습니다.",
                        icon: "success",
                        confirmButtonText: "확인",
                    }).then(function () {
                        document.location.reload();
                    });
                    */
                    alert('저장 되었습니다.');
                    document.location.reload();
                } else {
                    console.log(data.message);
                    /*
                    Swal.fire({
                        title: "Error",
                        text: data.message,
                        icon: "error",
                        confirmButtonText: "확인",
                    });
                    */
                    alert(data.message);
                }

            });
        } else {
            alert('URL IT NOT EXISTS');
        }

    } else {
        // 파일 업로드 체크
        fileUploadCheck();
        timeOut = setTimeout(function () {
            save_file(url);
        }, 500);
    }
}



// 저장시 조회 및 입력화면 삭제
function save_to_list(url) {
    alert('deprecated');
}

function setToolbar(str) {

    if (!str)
        return;

    if (typeof str === "string") {
        $("div.toolbar").html($.trim(str));
    } else {
        $("div.toolbar").html(str.join("<br>"));
    }


}

function goBack(e) {
    if (e)
        e.preventDefault();

    if ($("#popup-bg").length) {

        // callSearch();

        $("#popup-bg").hide(0, function () {
            $(this).remove();
        });
        return;
    }

    if (!$("#form-view").length && !$("#detail-view").length) {
        // datatables 의 tr 의 selected 클래스를 지움
        $("tr").removeClass("selected");
        return;
    }

    if ($("#form-view").length && $("#form-view:visible")) {
        swal({
            title: "수정화면을 닫으시겠습니까?",
            text: "수정한 내용이 있다면  저장되지않습니다.",
            icon: "warning",
            buttons: [
                '취소',
                '확인'
            ],
            dangerMode: true,
        }).then(function (isConfirm) {
            if (isConfirm) {
                $.when(
                    $("#form-view").remove()
                ).then(
                    function () {
                        if ($("#detail-view").length) {
                            $("#detail-view").show(0, function () {
                                $(".content-body").scrollTop(detailScrollTop);
                            });
                        } else {
                            $("section[role='main']>section").show(0, function () {
                                $(".content-body").scrollTop(listScrollTop);
                            });
                        }

                        callSearch();
                    }
                );
            }
        });

    } else {

        $.when(
            $("#detail-view").remove()
        ).then(function () {
                //callSearch();

                $("section[role='main']>section").show(0, function () {
                    $(".content-body").scrollTop(listScrollTop);
                });
            }
        );
    }
}

function toList(e) {
    if (e)
        e.preventDefault();

    if ($("#form-view").length && $("#form-view:visible")) {
        swal({
            title: "목록으로 이동 하시겠습니까?",
            text: "수정한 내용이 있다면  저장되지않습니다.",
            icon: "warning",
            buttons: [
                '취소',
                '확인'
            ],
            dangerMode: true,
        }).then(function (isConfirm) {
            if (isConfirm) {
                $.when(
                    $("#detail-view, #form-view, #popup-bg").remove()
                ).then(
                    $("section[role='main']>section").show(0, function () {
                        $(".content-body").scrollTop(listScrollTop);
                    })
                );
            }
        });
    } else {
        $.when(
            $("#detail-view, #form-view, #popup-bg").remove()
        ).then(
            $("section[role='main']>section").show(0, function () {
                $(".content-body").scrollTop(listScrollTop);
            })
        );

    }
}

function callSearch() {
    if ($("#detail-view").length && window.hasOwnProperty('search_sub') && typeof search_sub === "function") {
        search_sub.call();
    } else if (window.hasOwnProperty('search') && typeof search === "function") {
        search.call();
    }
}

function closeForm() {
    $.when(
        $("#form-view").remove()
    ).then(
        function () {
            if ($("#detail-view").length) {
                $("#detail-view").show(0, function () {
                    $(".content-body").scrollTop(detailScrollTop);

                    if (typeof searchSub == 'function') {
                        searchSub();
                    }
                });
            } else {
                $("section[role='main']>.panel").show(0, function () {
                    $(".content-body").scrollTop(listScrollTop);
                });
            }
        }
    );
}

let fileUpload = function () {
    return new Promise(function (resolve, reject) {
        let check_cnt = 0;
        let dropzone_size = $(".dropzone").size();

        isIng = true;
        isIng = true;
        isDone = false;

        console.log('dropzone_size: ' + dropzone_size);

        // 비동기를 표현하기 위해 setTimeout 함수를 사용
        if (dropzone_size == 0) {
            isDone = true;
            resolve("completed file upload [dropzone is not exists]");
        } else {
            $(".dropzone").each(function (index) {

                // console.debug('dropzone each start [' + index + ']');

                let dz = $(this)[0].dropzone;
                if (dz.getQueuedFiles().length === 0) {
                    dropzone_size--;
                    return true; // each continue
                }

                dz.on("queuecomplete", function () {
                    // console.debug("queuecomplete .. ");

                    check_cnt--;
                    if (check_cnt == 0) {
                        // 파일 업로드 애니메이션 표현시간 딜레이
                        setTimeout(function () {
                            isDone = true;
                            isIng = false;
                        }, 1000);
                        resolve("file upload completed");
                    }
                });

                dz.processQueue();
                check_cnt++;
            });

            if (dropzone_size == 0) {
                isDone = true;
                resolve("completed file upload [add files is not exists]");
            }
        }
    });
};

function fileUploadCheck() {
    if (!isIng) {
        fileUpload().then(function (result) {
            //성공
        }, function (err) {
            //실패
        });
    }
}

function urlPatternCheck(name) {

    // 입력 값에 대한 유효성을 검증한다
    // 파라미터는 name 으로 지정하고, 없을 경우를 대비하여 id도 체크한다.
    let url = $("input[name='" + name + "']").val() || $("#" + name).val();

    if (!url) {
        // 입력값이 없다면 리턴.
        return true;
    }

    let expression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
    let regex = new RegExp(expression);

    if (url.match(regex)) {
        return true;
    } else {

        alert("URL[ " + url + " ] 값이 유효하지 않습니다.");

        if ($("input[name='" + name + "']").length)
            $("input[name='" + name + "']").first().focus();

        if ($("#" + name))
            $("#" + name).focus();

        return false;
    }
}

function yyyymmddFormat(yyyymmdd) {
    yyyymmdd = String(yyyymmdd);
    return yyyymmdd.substring(0, 4) + "/" + yyyymmdd.substring(4, 6) + "/" + yyyymmdd.substring(6, 8);
}

function checkValue(str, max) {
    if (str.charAt(0) !== '0' || str == '00') {
        if (str.length > 2) str = str.substr(0, 2);

        let num = parseInt(str);
        if (isNaN(num) || num <= 0 || num > max) num = 0;
        str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
    }
    return str;
}

function setDatePicker() {
    return;
    
    setTimeout(function () {
        let $datepicker = $("input.datepicker:visible");

        console.debug("setDatePicker.size: " + $datepicker.size());

        // 달력 아이콘이 추가되어 있다면 리턴
        if ($datepicker.parent().find('.input-group-addon').length) {
            console.debug("setDatePicker return " + $datepicker.parent().find('.input-group-addon').length);
            return;
        }

        let calendarIcon = '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>';

        // console.debug('setDatePicker called ' + $datepicker.size())

        // $datepicker.parent().prepend(calendarIcon);
        $datepicker.datepicker({dateFormat: "yy/mm/dd"})
            .attr("placeholder", "YYYY/MM/DD")
            .attr("maxlength", "10")
            .keyup(function (e) {
                if (e.keyCode === 8)
                    return;

                this.type = 'text';
                let input = this.value;
                // if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
                let values = input.split('/').map(function (v) {
                    return v.replace(/\D/g, '')
                });

                if (values[1]) values[1] = checkValue(values[1], 12);
                if (values[2]) values[2] = checkValue(values[2], 31);

                let output = values.map(function (v, i) {
                    if (i == 0 && v.length >= 4) {
                        v = v.substr(0, 4) + '/';
                    }

                    if (i == 1 && v.length >= 2) {
                        v = v.substr(0, 2) + '/';
                    }
                    return v;
                });

                //console.log(output);

                this.value = output.join('').substr(0, 14);
            });

    }, 500);


    /*
    $datepicker.blur(function (e) {
        this.type = 'text';
        var input = this.value;
        var values = input.split('/').map(function (v, i) {
            return v.replace(/\D/g, '')
        });
        var output = '';

        if (values.length == 3) {
            var year = values[2].length !== 4 ? parseInt(values[2]) + 2000 : parseInt(values[2]);
            var month = parseInt(values[0]) - 1;
            var day = parseInt(values[1]);
            var d = new Date(year, month, day);
            if (!isNaN(d)) {
                document.getElementById('result').innerText = d.toString();
                var dates = [d.getMonth() + 1, d.getDate(), d.getFullYear()];
                output = dates.map(function (v) {
                    v = v.toString();
                    return v.length == 1 ? '0' + v : v;
                }).join(' / ');
            }
            ;
        }
        ;
        this.value = output;
    });
    */
}

function setDefaultDate(period) {
    let today = new Date();
    let year = today.getFullYear();
    let month = today.getMonth();
    let date = today.getDate();

    let yearago = new Date();

    switch (period) {
        case "1m":
            yearago.setMonth(month - 1);
            yearago.setDate(date + 1);
            break;
        case "1y":
            yearago.setFullYear(year - 1);
            yearago.setDate(date + 1);
            break;
        case "2y":
            yearago.setFullYear(year - 2);
            yearago.setDate(date + 1);
            break;
    }

    let date_start = $("input[name='date_start']").val();
    let date_end = $("input[name='date_end']").val();

    let datepicker_names = $(".datepicker").map(function () {
        return $(this).prop("name");
    });

    console.debug('datepicker_names check --- s');
    console.debug(datepicker_names);
    console.debug('datepicker_names check --- e');

    if (!date_start)
        $("input[name='date_start']").val(yearago.format('yyyy/MM/dd'));

    if (!date_end)
        $("input[name='date_end']").val(today.format('yyyy/MM/dd'));
}


String.prototype.startsWith = function (str) {
    if (this.length < str.length) {
        return false;
    }
    return this.indexOf(str) == 0;
}

function goList(){
    history.back();
}