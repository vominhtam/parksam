from django.db import models
from django.contrib.auth.models import User
from django_summernote.models import AbstractAttachment


# Create your models here.
class AttachFile(models.Model):
    group_name = models.CharField(max_length=255, blank=True, null=True)
    group_id = models.CharField(max_length=255, blank=True, null=True)
    real_name = models.CharField(max_length=255, blank=True, null=True)
    save_name = models.CharField(max_length=255, blank=True, null=True)
    ext = models.CharField(max_length=10, blank=True, null=True)
    real_size = models.IntegerField(blank=True, null=True)
    save_size = models.CharField(max_length=255, blank=True, null=True)
    save_path = models.CharField(max_length=255, blank=True, null=True)
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")
    regist_id = models.IntegerField(db_column='regist_id', blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(db_column='modify_id', blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'attach_file'

class SummernoteAttach(AbstractAttachment):
    test = models.CharField(max_length=100)

    def get(self, *args, **kwargs):
        super(SummernoteAttach, self).get(*args, **kwargs)

    def save(self, *args, **kwargs):

        super(SummernoteAttach, self).save(*args, **kwargs)