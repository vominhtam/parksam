import os
import uuid
import json
import datetime
import logging
import traceback
import subprocess
import re
import base64
import requests
import urllib
import mimetypes


from django.shortcuts import render, redirect, render_to_response
from django.views.decorators.csrf import csrf_exempt
from parksam.settings import MEDIA_ROOT, BASE_DIR, UPLOAD_DIR
from django.core.files.storage import FileSystemStorage
from common.models import AttachFile
from parksam.settings import BASE_DIR
from django.forms.models import model_to_dict
from django.http import JsonResponse, HttpResponse
from django.core.mail import send_mail
from django.template import RequestContext

log = logging.getLogger(__name__)


# Create your views here.


@csrf_exempt
def file_upload(request):
    def sizeof_fmt(num, suffix="b"):
        for unit in ["", "k", "m", "g", "t", "p", "e", "z"]:
            if abs(num) < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, "Yi", suffix)

    if request.method == "POST":
        if request.FILES:
            for f in request.FILES:
                item = request.FILES[f]
                name_enc = str(uuid.uuid4()).replace("-", "")

                if not os.path.exists(MEDIA_ROOT):
                    os.makedirs(MEDIA_ROOT)

                fs = FileSystemStorage()
                filename = fs.save(name_enc, item)
                uploaded_file_url = fs.url(filename)
                group_name = get_group_name(request.POST.get("path"))

                new_attach = AttachFile.objects.create(
                    group_name=group_name,
                    real_name=str(item.name).replace(",", "_"),
                    real_size=item.size,
                    save_name=name_enc,
                    save_size=sizeof_fmt(item.size),
                    save_path=uploaded_file_url.replace(str(BASE_DIR), ""),
                    ext=item.name.split(".")[-1],
                    regist_id=request.user.id,
                )
                new_attach.save()

                return JsonResponse(json.dumps(model_to_dict(new_attach)), safe=False)
        else:
            return JsonResponse(json.dumps({}), safe=False)

    # 수정화면 TEST
    a = TbTest.objects.get(pk=383)
    form = TestForm(instance=a)
    return render(request, "common/file_sample.html", {"form": form})


@csrf_exempt
def file_download(request, id):
    try:
        f = None
        file_path = None
        file_name = None
        file_ext = None

        # 요청이 ajax 이면 파일 유무를 확인하고 다운로드 기능을 수행하도록 함
        if id:
            file = AttachFile.objects.get(pk=id)
            file_path = os.path.join(BASE_DIR, file.save_path)
            file_name = "%s" % file.real_name

        else:
            pass

        # 실제 파일이 저장되는 위치로 설정 수정
        file_path = file_path.replace("/media/", "/root/workspace/parksam/static/upload/")
        log.debug("file_name [%s] file_path [%s]" % (file_name, file_path))

        if os.path.exists(file_path):
            log.debug("file is exists.")
            file_name = urllib.parse.quote(file_name.encode("utf-8"))

            with open(file_path, "rb") as fh:
                response = HttpResponse(fh.read(), content_type=mimetypes.guess_type(file_path)[0])
                response["Content-Disposition"] = "attachment;filename*=UTF-8''%s" % file_name
                return response

        else:
            log.debug("file is not exists. check path[%s]" % file_path)
            # logger.info("file not exists !! path[%s]" % file_path)
            # raise Http404

            return HttpResponse("<script>alert('파일이 존재하지 않습니다.');</script>")

    except Exception as e:
        log.error(traceback.format_exc(e))


def test():
    pass


def get_group_name(request):
    return "group_name"


@csrf_exempt
def send_mail_test(request):
    print("send_mail_test called")

    try:
        send_mail(
            "메일 발송 테스트",
            "<h1>태스 내용 표시 테스트1111111<h1>",
            "help@parksam.co.kr",
            ["redukyo@gmail.com"],
            html_message="<h1>태스 내용 표시 테스트2222222<h1>",
            fail_silently=False,
        )
        return JsonResponse({"success": True, "message": "메일을 발송 하였습니다."})
    except Exeption as e:

        return JsonResponse({"success": False, "message": e.message})


def send_sms(request):
    try:
        # sms api
        return JsonResponse({"success": True, "message": "SMS를 발송 하였습니다."})
    except Exeption as e:
        return JsonResponse({"success": False, "message": e.message})


def checkplus_main(request):
    # NICE평가정보에서 발급한 안심본인인증 서비스 개발정보 (사이트코드, 사이트패스워드)
    sitecode = "BU390"
    sitepasswd = "mOoRRGOYh7O8"

    # 안심본인인증 모듈의 절대경로 (권한:755, FTP업로드방식: 바이너리)
    # ex) cb_encode_path = 'C:\\module\\CPClient.exe'
    #     cb_encode_path = '/root/module/CPClient'
    cb_encode_path = os.path.join(BASE_DIR, "module/CPClient")

    host = request.META["HTTP_HOST"]

    print("host:::::::", host)

    # 실서버에서 도메인을 고정하여야함.. 도커와 관련된 환경 설정은 추가적으로 확인 필요
    # host = 'parksam.co.kr'
    if host == "web":
        returnurl = "https://www.parksam.co.kr/checkplus_success"
        errorurl = "https://www.parksam.co.kr/checkplus_fail"
    else:
        returnurl = "http://%s/checkplus_success" % host
        errorurl = "http://%s/checkplus_fail" % host

    # 인증성공 시 결과데이터 받는 리턴URL (방식:절대주소, 필수항목:프로토콜)

    # 인증실패 시 결과데이터 받는 리턴URL (방식:절대주소, 필수항목:프로토콜)

    # 팝업화면 설정
    authtype = ""  # 인증타입 (공백:기본 선택화면, X:공인인증서, M:핸드폰, C:카드)
    popgubun = "N"  # 취소버튼 (Y:있음, N:없음)
    customize = ""  # 화면타입 (공백:PC페이지, Mobile:모바일페이지)
    gender = ""  # 성별설정 (공백:기본 선택화면, 0:여자, 1:남자)

    # 요청번호 초기화
    # :세션에 저장해 사용자 특정 및 데이타 위변조 검사에 이용하는 변수 (인증결과와 함께 전달됨)
    reqseq = ""

    # 인증요청 암호화 데이터 초기화
    enc_data = ""

    # 처리결과 메세지 초기화
    returnMsg = ""

    # 요청번호 생성
    try:
        # 파이썬 버전이 3.5 미만인 경우 check_output 함수 이용
        #  reqseq = subprocess.check_output([cb_encode_path, 'SEQ', sitecode])
        reqseq = subprocess.run([cb_encode_path, "SEQ", sitecode], capture_output=True, encoding="euc-kr").stdout
    except subprocess.CalledProcessError as e:
        # check_output 함수 이용하는 경우 1 이외의 결과는 에러로 처리됨
        reqseq = e.output.decode("euc-kr")
        print("cmd:", e.cmd, "\n output:", e.output)
    finally:
        print("reqseq:", reqseq)

    # 요청번호 세션에 저장 (세션 이용하지 않는 경우 생략)
    # session["REQ_SEQ"] = reqseq

    # plain 데이터 생성 (형식 수정불가)
    plaindata = (
        "7:REQ_SEQ"
        + str(len(reqseq))
        + ":"
        + reqseq
        + "8:SITECODE"
        + str(len(sitecode))
        + ":"
        + sitecode
        + "9:AUTH_TYPE"
        + str(len(authtype))
        + ":"
        + authtype
        + "7:RTN_URL"
        + str(len(returnurl))
        + ":"
        + returnurl
        + "7:ERR_URL"
        + str(len(errorurl))
        + ":"
        + errorurl
        + "11:POPUP_GUBUN"
        + str(len(popgubun))
        + ":"
        + popgubun
        + "9:CUSTOMIZE"
        + str(len(customize))
        + ":"
        + customize
        + "6:GENDER"
        + str(len(gender))
        + ":"
        + gender
    )

    # 인증요청 암호화 데이터 생성
    try:
        # 파이썬 버전이 3.5 미만인 경우 check_output 함수 이용
        #  enc_data = subprocess.check_output([cb_encode_path, 'ENC', sitecode, sitepasswd, plaindata])
        enc_data = subprocess.run(
            [cb_encode_path, "ENC", sitecode, sitepasswd, plaindata], capture_output=True, encoding="euc-kr"
        ).stdout
    except subprocess.CalledProcessError as e:
        # check_output 함수 이용하는 경우 1 이외의 결과는 에러로 처리됨
        enc_data = e.output.decode("euc-kr")
        print("cmd:", e.cmd, "\n output:\n", e.output)
    finally:
        print("enc_data:\n", enc_data)

    # 화면 렌더링 변수 설정
    render_params = {}
    render_params["enc_data"] = enc_data
    render_params["returnMsg"] = returnMsg
    return enc_data, returnMsg
    # return render(request, "common/checkplus_main.html", render_params)


@csrf_exempt
def checkplus_success(request):
    print("called checkplus_success #############################################")

    # NICE평가정보에서 발급한 안심본인인증 서비스 개발정보 (사이트코드, 사이트패스워드)
    sitecode = "BU390"
    sitepasswd = "mOoRRGOYh7O8"

    # 안심본인인증 모듈의 절대경로 (권한:755, FTP업로드방식: 바이너리)
    # ex) cb_encode_path = 'C:\\module\\CPClient.exe'
    #     cb_encode_path = '/root/module/CPClient'
    cb_encode_path = os.path.join(BASE_DIR, "module/CPClient")

    # CP요청번호 초기화
    reqseq = ""

    # 인증결과 암호화 데이터 초기화
    enc_data = ""

    # 인증결과 복호화 데이터 초기화
    plaindata = ""

    # 처리결과 메세지 초기화
    returnMsg = ""

    # 인증결과 복호화 시간 초기화
    ciphertime = ""

    # 인증결과 데이터 초기화
    requestnumber = ""  # 요청번호
    responsenumber = ""  # 본인인증 응답코드 (응답코드 문서 참조)
    authtype = ""  # 인증수단 (M:휴대폰, c:카드, X:인증서, P:삼성패스)
    name = ""  # 이름 (EUC-KR)
    utfname = ""  # 이름 (UTF-8, URL인코딩)
    birthdate = ""  # 생년월일 (YYYYMMDD)
    gender = ""  # 성별 코드 (0:여성, 1:남성)
    nationalinfo = ""  # 내/외국인 정보 (0:내국인, 1:외국인)
    dupinfo = ""  # 중복가입확인값 (64Byte, 개인식별값, DI:Duplicate Info)
    conninfo = ""  # 연계정보확인값 (88Byte, 개인식별값, CI:Connecting Info)
    mobileno = ""  # 휴대폰번호
    mobileco = ""  # 통신사 (가이드 참조)

    # NICE에서 전달받은 인증결과 암호화 데이터 취득
    try:
        # GET 요청 처리
        if request.method == "GET":
            print("checkplus_success:GET")
            enc_data = request.GET.get("EncodeData")
        # POST 요청 처리
        else:
            print("checkplus_success:POST")
            enc_data = request.POST.get("EncodeData")
    except:
        print(traceback.format_exc())
    finally:
        print("enc_data:\n", enc_data)

    ################################### 문자열 점검 ######################################
    errChars = re.findall("[^0-9a-zA-Z+/=]", enc_data)
    if len(re.findall("[^0-9a-zA-Z+/=]", enc_data)) > 0:
        print("errChars=", errChars)
        return "문자열오류: 입력값 확인이 필요합니다"
    if (base64.b64encode(base64.b64decode(enc_data))).decode() != enc_data:
        return "변환오류: 입력값 확인이 필요합니다"
    #####################################################################################

    # checkplus_main에서 세션에 저장한 요청번호 취득 (세션 이용하지 않는 경우 생략)
    # try:
    #     reqseq = session["REQ_SEQ"]
    # except Exception as e:
    #     print("ERR: reqseq=", reqseq)
    # finally:
    #     print("reqseq:", reqseq)

    if enc_data != "":
        # 인증결과 암호화 데이터 복호화 처리
        try:
            # 파이썬 버전이 3.5 미만인 경우 check_output 함수 이용
            #  plaindata = subprocess.check_output([cb_encode_path, 'DEC', sitecode, sitepasswd, enc_data])
            plaindata = subprocess.run(
                [cb_encode_path, "DEC", sitecode, sitepasswd, enc_data], capture_output=True, encoding="euc-kr"
            ).stdout
        except subprocess.CalledProcessError as e:
            # check_output 함수 이용하는 경우 1 이외의 결과는 에러로 처리됨
            plaindata = e.output.decode("euc-kr")
            print("cmd:", e.cmd, "\n output:\n", e.output)
        finally:
            print("plaindata:\n", plaindata)
    else:
        returnMsg = "처리할 암호화 데이타가 없습니다."

    # 복호화 처리결과 코드 확인
    if plaindata == -1:
        returnMsg = "암/복호화 시스템 오류"
    elif plaindata == -4:
        returnMsg = "복호화 처리 오류"
    elif plaindata == -5:
        returnMsg = "HASH값 불일치 - 복호화 데이터는 리턴됨"
    elif plaindata == -6:
        returnMsg = "복호화 데이터 오류"
    elif plaindata == -9:
        returnMsg = "입력값 오류"
    elif plaindata == -12:
        returnMsg = "사이트 비밀번호 오류"
    else:
        # 요청번호 추출
        requestnumber = GetValue(plaindata, "REQ_SEQ")

        # 데이터 위변조 검사 (세션 이용하지 않는 경우 분기처리 생략)
        # : checkplus_main에서 세션에 저장한 요청번호와 결과 데이터의 추출값 비교하는 추가적인 보안처리
        # if reqseq == requestnumber:
        # 인증결과 복호화 시간 생성 (생략불가)
        try:
            # 파이썬 버전이 3.5 미만인 경우 check_output 함수 이용
            #  ciphertime = subprocess.check_output([cb_encode_path, 'CTS', sitecode, sitepasswd, enc_data])
            ciphertime = subprocess.run(
                [cb_encode_path, "CTS", sitecode, sitepasswd, enc_data], capture_output=True, encoding="euc-kr"
            ).stdout
        except subprocess.CalledProcessError as e:
            # check_output 함수 이용하는 경우 1 이외의 결과는 에러로 처리됨
            ciphertime = e.output.decode("euc-kr")
            print("cmd:", e.cmd, "\n output:", e.output)
        finally:
            print("ciphertime:", ciphertime)
        # else:
        #   returnMsg = '세션 불일치 오류'

    # 인증결과 복호화 시간 확인
    if ciphertime != "":

        #####################################################################################
        # 인증결과 데이터 추출
        # : 결과 데이터의 통신이 필요한 경우 암호화 데이터(EncodeData)로 통신 후 복호화 해주십시오
        #   복호화된 데이터를 통신하는 경우 데이터 유출에 주의해주십시오 (세션처리 권장)
        #####################################################################################

        responsenumber = GetValue(plaindata, "RES_SEQ")
        authtype = GetValue(plaindata, "AUTH_TYPE")
        name = GetValue(plaindata, "NAME")
        utfname = GetValue(plaindata, "UTF8_NAME")
        birthdate = GetValue(plaindata, "BIRTHDATE")
        gender = GetValue(plaindata, "GENDER")
        nationalinfo = GetValue(plaindata, "NATIONALINFO")
        dupinfo = GetValue(plaindata, "DI")
        conninfo = GetValue(plaindata, "CI")
        mobileno = GetValue(plaindata, "MOBILE_NO")
        mobileco = GetValue(plaindata, "MOBILE_CO")

        print("responsenumber:" + responsenumber)
        print("authtype:" + authtype)
        print("name:" + name)
        print("utfname:" + utfname)
        print("birthdate:" + birthdate)
        print("gender:" + gender)
        print("nationalinfo:" + nationalinfo)
        print("dupinfo:" + dupinfo)
        print("conninfo:" + conninfo)
        print("mobileno:" + mobileno)
        print("mobileco:" + mobileco)

        returnMsg = "사용자 인증 성공"

    # 화면 렌더링 변수 설정
    render_params = {}
    render_params["plaindata"] = plaindata
    render_params["returnMsg"] = returnMsg
    render_params["ciphertime"] = ciphertime
    render_params["requestnumber"] = requestnumber
    render_params["responsenumber"] = responsenumber
    render_params["authtype"] = authtype
    render_params["name"] = name
    render_params["utfname"] = utfname
    render_params["birthdate"] = birthdate
    render_params["gender"] = gender
    render_params["nationalinfo"] = nationalinfo
    render_params["dupinfo"] = dupinfo
    render_params["conninfo"] = conninfo
    render_params["mobileno"] = mobileno
    render_params["mobileco"] = mobileco
    # return render(request, "common/checkplus_success.html", render_params)
    return render(request, "common/checkplus_success_set.html", render_params)


@csrf_exempt
def checkplus_fail(request):
    print("called checkplus_fail #############################################")
    # NICE평가정보에서 발급한 안심본인인증 서비스 개발정보 (사이트코드, 사이트패스워드)
    sitecode = "BU390"
    sitepasswd = "mOoRRGOYh7O8"

    # 안심본인인증 모듈의 절대경로 (권한:755, FTP업로드방식: 바이너리)
    # ex) cb_encode_path = 'C:\\module\\CPClient.exe'
    #     cb_encode_path = '/root/module/CPClient'
    cb_encode_path = os.path.join(BASE_DIR, "module/CPClient")

    # 인증결과 암호화 데이터 초기화
    enc_data = ""

    # 인증결과 복호화 데이터 초기화
    plaindata = ""

    # 처리결과 메세지 초기화
    returnMsg = ""

    # 인증결과 복호화 시간 초기화
    ciphertime = ""

    # 인증결과 데이터 초기화
    requestnumber = ""  # 요청번호
    errcode = ""  # 본인인증 응답코드 (응답코드 문서 참조)
    authtype = ""  # 인증수단 (M:휴대폰, c:카드, X:인증서, P:삼성패스)

    # NICE에서 전달받은 인증결과 암호화 데이터 취득
    result = request.form
    try:
        enc_data = result["EncodeData"]
    except KeyError as e:
        print(e)
    finally:
        print("enc_data:\n", enc_data)

    ################################### 문자열 점검 ######################################
    errChars = re.findall("[^0-9a-zA-Z+/=]", enc_data)
    if len(re.findall("[^0-9a-zA-Z+/=]", enc_data)) > 0:
        print("errChars=", errChars)
        return "문자열오류: 입력값 확인이 필요합니다"
    if (base64.b64encode(base64.b64decode(enc_data))).decode() != enc_data:
        return "변환오류: 입력값 확인이 필요합니다"
    #####################################################################################

    if enc_data != "":
        try:
            # 인증결과 암호화 데이터 복호화 처리
            #  plaindata = subprocess.check_output([cb_encode_path, 'DEC', sitecode, sitepasswd, enc_data])
            plaindata = subprocess.run(
                [cb_encode_path, "DEC", sitecode, sitepasswd, enc_data], capture_output=True, encoding="euc-kr"
            ).stdout
        except subprocess.CalledProcessError as e:
            # check_output 함수 이용하는 경우 1 이외의 결과는 에러로 처리됨
            plaindata = e.output.decode("euc-kr")
            print("cmd:", e.cmd, "\n output:\n", e.output)
        finally:
            print("plaindata:\n", plaindata)
    else:
        returnMsg = "처리할 암호화 데이타가 없습니다."

    # 복호화 처리결과 코드 확인
    if plaindata == -1:
        returnMsg = "암/복호화 시스템 오류"
    elif plaindata == -4:
        returnMsg = "복호화 처리 오류"
    elif plaindata == -5:
        returnMsg = "HASH값 불일치 - 복호화 데이터는 리턴됨"
    elif plaindata == -6:
        returnMsg = "복호화 데이터 오류"
    elif plaindata == -9:
        returnMsg = "입력값 오류"
    elif plaindata == -12:
        returnMsg = "사이트 비밀번호 오류"
    else:
        # 인증결과 복호화 시간 생성
        try:
            # 파이썬 버전이 3.5 미만인 경우 check_output 함수 이용
            #  ciphertime = subprocess.check_output([cb_encode_path, 'CTS', sitecode, sitepasswd, enc_data])
            ciphertime = subprocess.run(
                [cb_encode_path, "CTS", sitecode, sitepasswd, enc_data], capture_output=True, encoding="euc-kr"
            ).stdout
        except subprocess.CalledProcessError as e:
            # check_output 함수 이용하는 경우 1 이외의 결과는 에러로 처리됨
            ciphertime = e.output.decode("euc-kr")
            print("cmd:", e.cmd, "\n output:", e.output)
        finally:
            print("ciphertime:", ciphertime)

        # 인증결과 데이터 추출
        requestnumber = GetValue(plaindata, "REQ_SEQ")
        errcode = GetValue(plaindata, "ERR_CODE")
        authtype = GetValue(plaindata, "AUTH_TYPE")

        print("requestnumber:" + requestnumber)
        print("errcode:" + errcode)
        print("authtype:" + authtype)

    # 화면 렌더링 변수 설정
    render_params = {}
    render_params["plaindata"] = plaindata
    render_params["returnMsg"] = returnMsg
    render_params["ciphertime"] = ciphertime
    render_params["requestnumber"] = requestnumber
    render_params["errcode"] = errcode
    render_params["authtype"] = authtype
    return render_template("checkplus_fail.html", **render_params)


# 인증결과 데이터 추출 함수
def GetValue(plaindata, key):
    value = ""
    keyIndex = -1
    valLen = 0

    # 복호화 데이터 분할
    arrData = plaindata.split(":")
    cnt = len(arrData)
    for i in range(cnt):
        item = arrData[i]
        itemKey = re.sub("[\d]+$", "", item)

        # 키값 검색
        if itemKey == key:
            keyIndex = i

            # 데이터 길이값 추출
            valLen = int(item.replace(key, "", 1))

            if key != "NAME":
                # 실제 데이터 추출
                value = arrData[keyIndex + 1][:valLen]
            else:
                # 이름 데이터 추출 (한글 깨짐 대응)
                value = re.sub("[\d]+$", "", arrData[keyIndex + 1])

            break

    return value


# Encode string data
def stringToBase64(s):
    return base64.b64encode(s.encode("utf-8"))


def sms_oauth():
    try:
        sms_id = "parksamsms"
        api_key = "86d04e346e21441f0009269945c9142f"
        base64encodedString = stringToBase64("%s:%s" % (sms_id, api_key))
        print("base64encodedString:", base64encodedString)
        url = "https://sms.gabia.com/oauth/token"
        payload = "grant_type=client_credentials"
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Basic %s" % base64encodedString.decode("utf-8"),
        }
        response = requests.request(
            "POST",
            url,
            headers=headers,
            data=payload,
            allow_redirects=False,
            timeout=1000,
        )
        print("response.text [%s]" % response.text)
        return response.text
    except Exception as e:
        print(traceback.format_exc())
        return "error"


@csrf_exempt
def sms_send_list(request):
    sms_phone_user = request.POST.get("sms_phone_user")
    sms_subject_user = request.POST.get("sms_subject_user")
    sms_message_user = request.POST.get("sms_message_user")

    if sms_phone_user and sms_subject_user and sms_message_user:
        phone_list = sms_phone_user.split(",")
        for p in phone_list:
            status_code = sms_send(phone=p, subject=sms_subject_user, message=sms_message_user)

            if status_code != 200:
                return JsonResponse({"success": False, "phone_number": p})

    sms_phone_teacher = request.POST.get("sms_phone_teacher")
    sms_subject_teacher = request.POST.get("sms_subject_teacher")
    sms_message_teacher = request.POST.get("sms_message_teacher")

    if sms_phone_teacher and sms_subject_teacher and sms_message_teacher:
        status_code = sms_send(
            phone=sms_phone_teacher,
            subject=sms_subject_teacher,
            message=sms_message_teacher,
        )

        if status_code != 200:
            return JsonResponse({"success": False, "phone_number": sms_phone_teacher})

    return JsonResponse({"success": True})


@csrf_exempt
def sms_send(phone=None, subject=None, message=None):
    # send_message_type 가 없다면 종료
    if not phone or not subject or not message:
        return JsonResponse({"success": False, "message": "sms_send 발송 기능 확인이 필요합니다."})

    phone = str(phone).replace("-", "")

    if len(message) > 90:
        url = "https://sms.gabia.com/api/send/lms"
    else:
        url = "https://sms.gabia.com/api/send/sms"

    log.debug("len(message) [%s]" % len(message))
    log.debug("url [%s]" % url)

    # 제목과 같이 발송을 위하여 lms 로 고정
    url = "https://sms.gabia.com/api/send/lms"

    try:
        access_token = sms_oauth()
        d = json.loads(access_token)
        access_token = d.get("access_token")
        sms_id = "parksamsms"
        base64encodedString = stringToBase64("%s:%s" % (sms_id, access_token))
        refkey = datetime.datetime.now().strftime("%y%m%d%H%M%S")
        payload = "phone=%s&callback=0222872386&message=%s&refkey=[[%s_%s]]&subject=%s" % (
            phone,
            message,
            phone,
            refkey,
            subject,
        )
        payload = payload.encode("UTF-8")
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Basic %s" % base64encodedString.decode("utf-8"),
        }
        response = requests.request(
            "POST",
            url,
            headers=headers,
            data=payload,
            allow_redirects=False,
            timeout=1000,
        )
        return response.status_code
    except Exception as e:
        print("sms 발송 실패")
        log.debug("send sms fail ", traceback.format_exc())
        return JsonResponse({"success": False, "message": traceback.format_exc()})


def sms_messages(
    to=None,
    no=None,
    user_name="",
    teacher_name="",
    counsel_date="",
    counsel_time="",
    result_date="",
    zoom_link="",
    zoom_id="",
    zooom_pw="",
):

    message = ""

    # 발송대상이나 템플릿 번호가 없다면 리턴
    if not to or not no:
        return message

    if to == "user":
        if no == 1:
            message = """
                [박샘, 컨설턴트 및 일정확정]
                {teacher_name} 선생님이 배정 되었습니다. 
                첫 상담시간은 {counsel_date} {counsel_time} 입니다.

                상담 7일 전까지 학교생활기록부와 모의고사성적표를 올려주세요 
                (마이 페이지 -> 상담서류등록)
            """.format(
                teacher_name=teacher_name, counsel_date=counsel_date, counsel_time=counsel_time
            )
        elif no == 2:
            message = """
                [박샘, 컨설팅 강의실 접속 링크]
                1. 일정: {counsel_date} {counsel_time}
                2. 컨설턴트: {teacher_name} 선생님
                3. 강의실 입장하기
                - 접속 URL: {zoom_link}
                - ID: {zoom_id}
                - PW: {zoom_pw}           
            """.format(
                counsel_date=counsel_date,
                counsel_time=counsel_time,
                teacher_name=teacher_name,
                zoom_link=zoom_link,
                zoom_id=zoom_id,
                zoom_pw=zoom_pw,
            )
        elif no == 4:
            message = """
                [박샘, 회회원님 상담은 어떠셨나요?]
                {teacher_name} 선생님의 상담결과 보고서가 도착했습니다.

                상담결과를 확인하고 다음상담 전까지 주력점을 확인해보세요.
                (마이페이지 -> 상담내역확인)
            """.format(
                teacher_name=teacher_name
            )
        elif no == 5:
            message = """
                [박샘, 선생님의 답변 도착]
                {teacher_name} 선생님의 답변이 등록되었습니다. 확인하여 주세요.
            """.format(
                teacher_name=teacher_name
            )

    elif to == "teacher":
        if no == 1:
            message = """
                [박샘, 상담일정 안내]
                {user_name} 고객님과의 첫 상담일정이 확정되었습니다. 
                {counsel_date} {counsel_time}

                상담 7일 전까지 줌 링크를 생성, 고객님께 문자로 발송하여 주세요 
                (선생님용 웹사이트 -> 상담신청관리)            
            """
        elif no == 3:
            message = """
                [박샘, 서류가 등록되었습니다]
                {counsel_date} 상담예정인 {user_name} 고객님의 상담서류가 등록되었습니다.

                미리 확인 후 상담을 준비하여 주세요.            
            """.format(
                counsel_date=counsel_date, user_name=user_name
            )
        elif no == 4:
            message = """
                [박샘, 상담이 종료되었습니다]
                {result_date}까지 상담내역을 입력해주세요             
            """
        elif no == 5:
            message = """
                [박샘, 일대일 질문 등록]
                {user_name} 고객님의 질문이 등록되었습니다. 확인 후 답변을 등록해 주세요.            
            """.format(
                user_name=user_name
            )
        pass

    if message == "":
        message = "message가 올바르지 않습니다."

    return message


@csrf_exempt
def terms(request):
    return render(request, "common/terms.html")


@csrf_exempt
def privacy(request):
    return render(request, "common/privacy.html")


# 400
def bad_request_page(request, exception):
    return render(request, "common/page_400.html", status=400)


# 404
def page_not_found_page(request, exception):
    return render(request, "common/page_404.html", status=404)


# 500
def server_error_page(request):
    return render(request, "common/page_500.html", status=500)
