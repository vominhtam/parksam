from django.db import models
from django.contrib.auth.models import User

# Create your models here.

# 월간 입시뉴스
class BoardNews(models.Model):
    title = models.CharField(max_length=100, blank=False)
    content = models.TextField(blank=False)
    order_no = models.IntegerField(blank=True, null=True, default=1)
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")
    fix_yn = models.CharField(max_length=1, blank=True, null=True, default="N")
    regist_id = models.IntegerField(blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True, auto_now=True)

    class Meta:
        db_table = "board_news"


# 박샘 소식
class BoardNotice(models.Model):
    title = models.CharField(max_length=100, blank=False)
    content = models.TextField(blank=False)
    order_no = models.IntegerField(blank=True, null=True, default=1)
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")
    fix_yn = models.CharField(max_length=1, blank=True, null=True, default="N")
    regist_id = models.IntegerField(blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True, auto_now=True)

    class Meta:
        db_table = "board_notice"


# 자주묻는질문
class BoardFaq(models.Model):
    parent_id = models.IntegerField()
    title = models.CharField(max_length=100, blank=False)
    content = models.TextField(blank=False)
    order_no = models.IntegerField()
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")
    regist_id = models.IntegerField(blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True, auto_now=True)

    class Meta:
        db_table = "board_faq"


# 상담후기
class CounselReview(models.Model):
    title = models.CharField(max_length=500, blank=False)
    content = models.TextField(blank=False)
    order_no = models.IntegerField(blank=True, null=True, default=0)
    use_yn = models.CharField(max_length=1, blank=True, null=True, default="Y")
    regist_id = models.IntegerField(blank=True, null=True)
    regist_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    modify_id = models.IntegerField(blank=True, null=True)
    modify_date = models.DateTimeField(blank=True, null=True, auto_now=True)

    class Meta:
        db_table = "counsel_review"
