from django import forms
from data.models import BoardNews, BoardNotice, BoardFaq, CounselReview
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget


class BoardNewsForm(forms.ModelForm):
    use_yn_choices = [(True, '사용'), (False, '미사용')]
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    # use_yn = forms.BooleanField(initial=True)
    use_yn = forms.ChoiceField(choices=use_yn_choices, widget=forms.RadioSelect, initial=True)

    class Meta:
        model = BoardNews
        fields = ['id', 'title', 'content', 'order_no', 'fix_yn', 'use_yn']
        widgets = {
            'content': SummernoteWidget(),
        }


class BoardNoticeForm(forms.ModelForm):
    use_yn_choices = [('Y', '사용'), ('N', '미사용')]
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    # use_yn = forms.BooleanField(initial=True)
    use_yn = forms.ChoiceField(choices=use_yn_choices, widget=forms.RadioSelect, initial=True)

    class Meta:
        model = BoardNotice
        fields = "__all__"
        widgets = {
            'content': SummernoteWidget(),
        }


class CounselReviewForm(forms.ModelForm):
    use_yn_choices = [('Y', '사용'), ('N', '미사용')]
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    # use_yn = forms.BooleanField(initial=True)
    use_yn = forms.ChoiceField(choices=use_yn_choices, widget=forms.RadioSelect, initial=True)

    class Meta:
        model = CounselReview
        fields = "__all__"
        widgets = {
            'content': SummernoteWidget(),
        }


