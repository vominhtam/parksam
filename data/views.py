from common.models import AttachFile
from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from data.models import BoardNews, CounselReview
from data.forms import BoardNewsForm, CounselReviewForm
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.core.paginator import Paginator
from django.conf import settings
from mypage.models import BillingHistory


@login_required
def news_new(request):
    is_regular_paid = BillingHistory.objects.filter(use_yn="Y", billing_type="regular", user=request.user).exists()

    if not is_regular_paid:
        return redirect("/")

    user = request.user
    if not user.is_staff:
        # 접근권한이 없음을 안내하고 index 로 보내도록 기능 추가
        return redirect("/")

    if request.is_ajax():
        form = BoardNewsForm(request.POST, initial={"order_no": 1, "fix_yn": False, "use_yn": True})
        if form.is_valid():
            form.save()
            return JsonResponse({"success": True})
        else:
            return JsonResponse({"success": False, "message": form.errors})

    form = BoardNewsForm()
    context = {"form": form}

    return render(request, "data/news_new.html", context)


def news_list(request):
    # 정기상담 회원만 상세보기로 이동 가능하도록 함
    if request.user.is_anonymous:
        is_regular_paid = False
    else:
        is_regular_paid = BillingHistory.objects.filter(use_yn="Y", billing_type="regular", user=request.user).exists()

    page = request.GET.get("page", 1)
    all_list = BoardNews.objects.filter(use_yn="Y").order_by("-id")
    paginator = Paginator(all_list, settings.PAGE_SIZE)
    page_list = paginator.get_page(page)

    for news in page_list:
        print(news.id)

        files = AttachFile.objects.filter(group_name="news", group_id=news.id)
        if files:
            print("add files >> ", news.id, len(files))
            news.files = files

    context = {"page_list": page_list, "is_regular_paid": is_regular_paid}
    return render(request, "data/news_list.html", context)


def column_list(request):
    # 정기상담 회원만 상세보기로 이동 가능하도록 함
    if request.user.is_anonymous:
        is_regular_paid = False
    else:
        is_regular_paid = BillingHistory.objects.filter(use_yn="Y", billing_type="regular", user=request.user).exists()

    page = request.GET.get("page", 1)
    all_list = BoardNews.objects.filter(use_yn="Y").order_by("-id")
    paginator = Paginator(all_list, settings.PAGE_SIZE)
    page_list = paginator.get_page(page)
    context = {"page_list": page_list, "is_regular_paid": is_regular_paid}
    return render(request, "data/column_list.html", context)


@login_required
def news_detail(request, id):
    news = BoardNews.objects.get(id=id)
    form = BoardNewsForm(instance=news)

    files = AttachFile.objects.filter(group_name="news", group_id=news.id)

    context = {"form": form, "news": news, "files": files}
    return render(request, "data/news_detail.html", context)


@login_required
def news_form(request, id):
    is_regular_paid = BillingHistory.objects.filter(use_yn="Y", billing_type="regular", user=request.user).exists()

    if not is_regular_paid:
        return redirect("/")

    news = BoardNews.objects.get(id=id)

    print("news_form id [%s]" % id)

    if request.is_ajax():
        form = BoardNewsForm(request.POST, instance=news)
        if form.is_valid():
            form.save()
            return JsonResponse({"success": True})
        else:
            return JsonResponse({"success": False, "message": form.errors})

    form = BoardNewsForm(instance=news)

    context = {"form": form}
    return render(request, "data/news_form.html", context)


def counsel_review_list(request):

    if request.user.is_anonymous:
        is_paid = False
    else:
        is_paid = BillingHistory.objects.filter(use_yn="Y", user=request.user).exists()

    page = request.GET.get("page", 1)
    all_list = (
        CounselReview.objects.filter(use_yn="Y")
        .extra(
            select={
                "userid": "select rpad(substring(username, 1, 3), length(username), '*') from auth_user where auth_user.id = counsel_review.regist_id"
            }
        )
        .order_by("regist_date", "id")
    )
    paginator = Paginator(all_list, settings.PAGE_SIZE)
    page_list = paginator.get_page(page)

    context = {"page_list": page_list, "is_paid": is_paid}
    return render(request, "data/counsel_review_list.html", context)


@login_required
def counsel_review_new(request):
    if request.is_ajax():
        form = CounselReviewForm(request.POST)
        if form.is_valid():
            counsel_review = form.save(commit=False)
            counsel_review.regist_id = request.user.id
            counsel_review.save()
            return JsonResponse({"success": True})
        else:
            return JsonResponse({"success": False, "message": form.errors})

    form = CounselReviewForm(initial={"use_yn": "Y"})
    context = {"form": form}
    return render(request, "data/counsel_review_new.html", context)

@login_required
def counsel_review_form(request, id):
    counsel_review = CounselReview.objects.get(pk=id)
    if request.is_ajax():
        form = CounselReviewForm(request.POST, instance=counsel_review)
        if form.is_valid():
            counsel_review = form.save(commit=False)
            counsel_review.regist_id = request.user.id
            counsel_review.save()
            return JsonResponse({"success": True})
        else:
            return JsonResponse({"success": False, "message": form.errors})
    
    form = CounselReviewForm(instance=counsel_review)
    context = {"form": form, "counsel_review": counsel_review}
    return render(request, "data/counsel_review_form.html", context)    
