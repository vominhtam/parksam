from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class UserAddinfo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, db_column="user_id", verbose_name='내부 아이디')
    name = models.CharField(max_length=100, blank=False, verbose_name='이름')
    phone = models.CharField(max_length=13, blank=False, verbose_name='핸드폰 번호')
    birth_day = models.CharField(max_length=13, blank=True, null=True, verbose_name='생년월일')
    postcode = models.CharField(max_length=5, blank=False, verbose_name='우편번호')
    address1 = models.CharField(max_length=100, blank=False, verbose_name='주소1')
    address2 = models.CharField(max_length=100, blank=False, verbose_name='주소2')
    grade = models.CharField(max_length=13, blank=True, null=True, verbose_name='자녀학년')
    school = models.CharField(max_length=100, blank=True, null=True, verbose_name='자녀학교')
    source_from = models.CharField(max_length=50, blank=True, null=True, verbose_name='방문경로')
    naeiledu_id = models.CharField(max_length=50, blank=True, null=True, verbose_name='내일교육 아이디')
    use_marketing_yn = models.CharField(max_length=1, blank=False, verbose_name='정보활용 동의')
    reset_key = models.CharField(max_length=30, blank=True, null=True, verbose_name='비밀번호 초기화 키')
    teacher_id = models.IntegerField(blank=True, null=True, verbose_name='담당선생님')
    di = models.CharField(max_length=64, blank=False, verbose_name='실명인증 DI 값')

    class Meta:
        db_table = "auth_user_addinfo"


# Create your models here.
class UserTeacher(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, db_column="user_id")
    name = models.CharField(max_length=100, blank=False)
    phone = models.CharField(max_length=13, blank=False)

    class Meta:
        db_table = "auth_user_teacher"
