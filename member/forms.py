from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class UserForm(UserCreationForm):
    source_from_choices = (
        ("1", "홍보문자"),
        ("2", "내일교육 지면광고"),
        ("3", "내일신문 지면광고"),
        ("4", "지인추천"),
    )

    # email = forms.EmailField(label="이메일")

    # 추가 입력 항목 정의
    name = forms.CharField(label="이름", required=True)
    email = forms.CharField(label="이메일", required=True)
    phone1 = forms.CharField(label="핸드폰 번호1", required=True)
    phone2 = forms.CharField(label="핸드폰 번호2", required=True)
    phone3 = forms.CharField(label="핸드폰 번호3", required=True)
    # birth_year = forms.CharField(label="생년월일1", required=True)
    # birth_month = forms.CharField(label="생년월일2", required=True)
    # birth_day = forms.CharField(label="생년월일3", required=True)
    postcode = forms.CharField(label="우편번호", required=True)
    address1 = forms.CharField(label="주소", required=True)
    address2 = forms.CharField(label="상세주소", required=True)
    grade = forms.CharField(label="학년", required=False)
    school = forms.CharField(label="학교", required=False)
    source_from = forms.ChoiceField(label="방문경로", required=False, choices=source_from_choices)
    naeiledu_id = forms.CharField(label="내일교육 아이디", required=False)
    marketing_agreement_yn = forms.CharField(label="마케팅 정보 수신 동의", required=False)

    class Meta:
        model = User
        fields = ("username", "email")


class UserTeacherForm(UserCreationForm):
    email = forms.EmailField(label="이메일")

    # 추가 입력 항목 정의
    name = forms.CharField(label="이름", required=True)
    phone1 = forms.CharField(label="핸드폰 번호1", required=True)
    phone2 = forms.CharField(label="핸드폰 번호2", required=True)
    phone3 = forms.CharField(label="핸드폰 번호3", required=True)

    class Meta:
        model = User
        fields = ("username", "email")


class FindIdForm(forms.Form):
    name = forms.CharField(label="이름", required=True)
    phone1 = forms.CharField(label="전화번호1", required=True)
    phone2 = forms.CharField(label="전화번호2", required=True)
    phone3 = forms.CharField(label="전화번호3", required=True)


class FindPwForm(forms.Form):
    username = forms.CharField(label="아이디", required=True)
    name = forms.CharField(label="이름", required=True)
    phone1 = forms.CharField(label="전화번호1", required=True)
    phone2 = forms.CharField(label="전화번호2", required=True)
    phone3 = forms.CharField(label="전화번호3", required=True)
