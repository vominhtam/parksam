from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from member.forms import UserForm, UserTeacherForm, FindIdForm, FindPwForm
from django.contrib.auth.models import User
from member.models import UserAddinfo, UserTeacher
from django.http import JsonResponse
import traceback
from django.contrib.auth.hashers import make_password
import random
import string
from django.db.models import Q
from django.contrib.auth.decorators import login_required
import json
from common.views import checkplus_main


def signup(request):
    print("called signup")
    """
    계정생성
    """
    if request.is_ajax():
        form = UserForm(request.POST)
        is_valid = form.is_valid()

        if form.is_valid():
            form.save()

            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")

            try:
                # 회원가입의 추가 정보 저장
                UserAddinfo.objects.create(
                    user=User.objects.get(username=username),
                    name=form.cleaned_data.get("name"),
                    phone="%s-%s-%s"
                    % (
                        form.cleaned_data.get("phone1"),
                        form.cleaned_data.get("phone2"),
                        form.cleaned_data.get("phone3"),
                    ),
                    # birth_day="%s/%s/%s"
                    # % (
                    #     form.cleaned_data.get("birth_year"),
                    #     form.cleaned_data.get("birth_month"),
                    #     form.cleaned_data.get("birth_day"),
                    # ),
                    # postcode=form.cleaned_data.get("postcode"),
                    postcode=form.cleaned_data.get("postcode"),
                    address1=form.cleaned_data.get("address1"),
                    address2=form.cleaned_data.get("address2"),
                    # grade=form.cleaned_data.get("grade"),
                    # school=form.cleaned_data.get("school"),
                    # source_from=form.cleaned_data.get("source_from"),
                    naeiledu_id=form.cleaned_data.get("naeiledu_id"),
                    use_marketing_yn=form.cleaned_data.get("marketing_agreement_yn", "n"),
                )

                user = authenticate(username=username, password=raw_password)
                login(request, user)

                # next url 이 있다면 해당 url 로 이동
                next_url = request.POST.get("next")

                return JsonResponse({"result": is_valid, "next": next_url})
            except Exception as e:
                print(e)
                User.objects.get(username=username).delete()
                return JsonResponse({"result": False, "message": "회원가입이 실패하였습니다."})
        else:
            return JsonResponse({"result": is_valid, "message": form.errors})
        # username, email, phone 값이 중복이 있는지 확인하여 리턴
        """
        username = request.POST.get("username")
        email = request.POST.get("email")
        phone = "%s-%s-%s" % (
            form.cleaned_data.get("phone1"),
            form.cleaned_data.get("phone2"),
            form.cleaned_data.get("phone3"),
        )

        cnt1 = User.objects.filter(username=username).count()
        if cnt1 > 0:
            return JsonResponse({"result": False, "message": "아이디가 존재합니다."})

        cnt2 = User.objects.filter(email=email).count()
        if cnt2 > 0:
            return JsonResponse({"result": False, "message": "이메일이 존재합니다."})
            
        cnt3 = UserAddinfo.objects.filter(phone=phone).count()
        if cnt3 > 0:
            return JsonResponse({"result": False, "message": "전화번호가 존재합니다."})
        """

    else:
        print("signup case 3")
        next_url = request.GET.get("next", "")

        enc_data, returnMsg = checkplus_main(request)
        form = UserForm()
        context = {"form": form, "enc_data": enc_data, "returnMsg": returnMsg, "next": next_url}

    return render(request, "common/signup.html", context)


def find_info(request):
    return render(request, "common/find_info.html")


def user_check(request):
    username = request.POST.get("username")
    password = request.POST.get("password")

    user1 = User.objects.filter(username=username)

    if not user1:
        return JsonResponse({"success": False, "message": "%s 사용자 정보가 없습니다." % username})

    user2 = authenticate(username=username, password=password)

    if not user2:
        return JsonResponse({"success": False, "message": "비밀번호가 일치하지 않습니다."})

    return JsonResponse({"success": True})


@login_required
def get_info_for_billing(request):
    user_id = request.user.id
    user = (
        User.objects.filter(id=user_id)
        .extra(
            select={
                "name": "select name from auth_user_addinfo where auth_user_addinfo.user_id = auth_user.id",
                "phone": "select phone from auth_user_addinfo where auth_user_addinfo.user_id = auth_user.id",
                "address": "select concat(address1, ' ', address2) from auth_user_addinfo where auth_user_addinfo.user_id = auth_user.id",
                "postcode": "select postcode from auth_user_addinfo where auth_user_addinfo.user_id = auth_user.id",
            }
        )
        .values("email", "name", "phone", "address", "postcode")
        .first()
    )
    return JsonResponse(user)


def find_id(request):
    if request.is_ajax():
        form = FindIdForm(request.POST)
        is_valid = form.is_valid()

        if not is_valid:
            return JsonResponse({"success": is_valid, "message": form.errors})
        else:
            name = form.cleaned_data.get("name")
            phone = "%s-%s-%s" % (
                form.cleaned_data.get("phone1"),
                form.cleaned_data.get("phone2"),
                form.cleaned_data.get("phone3"),
            )

            try:
                u = User.objects.get(useraddinfo__name=name, useraddinfo__phone=phone)
                username = u.username
                size = len(username)
                username = username[: size - 3] + "***"
                return JsonResponse({"success": is_valid, "message": username})
            except:
                return JsonResponse({"success": is_valid, "message": "일치하는 회원이 없습니다."})

    form = FindIdForm()
    return render(request, "common/find_id.html", {"form": form})


def find_pw(request):
    if request.is_ajax():
        form = FindPwForm(request.POST)
        is_valid = form.is_valid()

        if not is_valid:
            return JsonResponse({"success": is_valid, "message": form.errors})
        else:
            username = form.cleaned_data.get("username")
            name = form.cleaned_data.get("name")
            phone = "%s-%s-%s" % (
                form.cleaned_data.get("phone1"),
                form.cleaned_data.get("phone2"),
                form.cleaned_data.get("phone3"),
            )

            try:
                u = User.objects.get(
                    username=username, useraddinfo__name=name, useraddinfo__phone=phone
                )
                # 비밀번호는 복호화가 안되므로 초기화 시킨후 임시 비밀번호를 안내하여야 함
                # password_characters = string.ascii_letters + string.digits + string.punctuation
                password_characters = string.ascii_letters + string.digits

                password = "".join(random.choice(password_characters) for i in range(6))
                print("Random password is1:", password)

                hashed_password = make_password(password)
                print("Random password is2:", hashed_password)

                User.objects.filter(username=username).update(password=hashed_password)
                # 초기화된 비밀번호 핸드폰 번호로 전송 (링크 전송으로 할경우 smtp 설정 후 작업 예정)
                return JsonResponse({"success": is_valid})
            except Exception as e:
                print(traceback.print_exc())
                return JsonResponse({"success": is_valid, "message": "일치하는 회원이 없습니다."})

    form = FindPwForm()
    return render(request, "common/find_pw.html", {"form": form})
